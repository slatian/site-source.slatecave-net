+++
	title = "Slatians Blog"
	description = "Technical explorations, Minddumps, Opinions and whatever I'm interested in."
	sort_by = "date"
	template = "blog-index.html"
	page_template = "blog-page.html"
	generate_feeds = true
	[extra]
	show_feed_link = true
	short_title = "Blog"
	hide_description = true
+++


