+++
	title = "You should read: Outrage Warps Reality"
	description = "Steffo and Finnley on how mass paranoia and uninformed outrage hurt the Fediverse."

	[extra]
	banner = "dont_panic"
+++

## An Article I Think is Worth Reading

{% linkbutton(href="https://steffo.blog/outrage-warps-reality/", icon="action-next") %}Outrage Warps Reality{% end %}

> How mass paranoia and uninformed outrage will hurt the Fediverse, and what we can learn about the drama regarding VLC's AI and Google's newest service.

About a good 10 minute read.

{% chat() %}
: /resources/emoji/slatian_bean/slatian_bean_happy.png ::: Note from Slatian
> Don't let the note at the start of the post fool you. [Steffo](https://steffo.blog) and [Finnley](https://finnley.dev) did a very good job writing it!

> Given the current context this article has become even more relevant since it was published <time datetime="2025-02-15" title="2025-02-15">around 2 weeks ago</time>.

> Why are you still reading here anyway? [Go and read the post!](https://steffo.blog/outrage-warps-reality/)
{% end %}




