+++
	title = "Thunderbird: Use S/MIME Certificates from LDAP"
	description = "How to get loading public certificates from an LDAP Server to work on Thunderbird."
	updated = 2025-02-17

	[taxonomies]
	topics = ["Networking", "LDAP"]
+++

## Why combine S/MIME and LDAP?

Signing E-Mail with [S/MIME](https://en.wikipedia.org/wiki/S/MIME) has some big advantages if you are part of a bigger organisation, network or government. One specialised Team can take care of generating all the Certificates from a Certificate Authority. Everyone trusts that Team to handle giving out certificates correctly and by extension that Certificate Authority and with one Certificate import one can verify signatures from everyone in the organisation, great.

But if one has data that is worth signing it is probably also worth encrypting, which is where there is one little problem for the less technical users: One needs the public key of the receiver. Which can be imported from a file or obtained from a signed message.

{% chat() %}
: /resources/emoji/neofox/neofox_laptop_notice.png ::: Neofox asks critically
> You are telling me that I have to get a certificate file and import it every time I write someone new?

> Do you have any idea of how many people I have to write every day?!
>
> I don't have time for your little encrypting toy!
{% end %}

Importing certificates is hard.

But so is importing contacts, some organisations have an LDAP Address book to distribute their contact lists. Conveniently S/MIME certificates can also be distributed via an LDAP Address book (using the `userCertificate` field to be specific). This way the people minding their daily business can rely on the Mail client automatically figuring out who can receive encrypted Mail and with which Certificate.

<b>Note:</b> Trusting a central authority for encrypted communication isn't ideal, but it is good enough for within organisations where different departments already have to rely on each other to do their jobs and a **huge** improvement over not encrypting at all.

## How to set it up with Thunderbird?

With Thunderbird one can either manually add an LDAP Address book and install the certificates (there are enough guides for this already).

Or automate the process:

For installing the CAs Certificates as an admin the best way is to use the Enterprise Policies with the [`Certificates -> Install` rule](https://github.com/thunderbird/policy-templates/tree/master/templates/central#certificates--install).

For the address book an admin can preconfigure a [`user.js` file](https://kb.mozillazine.org/User.js_file) with the [appropriate LDAP settings](https://enterprise.thunderbird.net/manage-updates-policies-and-customization/thunderbird-enterprise-tips#setup-users-to-access-ldap-information-from-the-address-book).

## I have the Address book, but Thunderbird doesn't find the S/MIME Certificates?

If you have the LDAP Address book working, but the S/MIME Certificates seemingly can't be read from them:

Thunderbird doesn't automatically download certificates from Directory Servers that aren't the autocomplete directory Server (this is either a global setting or one bound to the E-Mail account).

<b>Note:</b> This is probably a privacy feature to not leak addresses to LDAP Servers one doesn't already trust, but this seems like black magic if one comes from the the "Why is S/MIME broken?" angle and not really explained anywhere.

If you are configuring via JavaScript those two are the important settings:
```
defaultPref('ldap_2.autoComplete.directoryServer','ldap_2.servers.AutoGEN-CompanyNameLDAP');
defaultPref('ldap_2.autoComplete.useDirectory',true);
```
(The example is taken from the LDAP settings tutorial linked in the previous section.)

In the global GUI settings you are looking for <samp>Composition</samp> > <samp>Addressing</samp> > <samp>When addressing messages, look for matching entries in:</samp> > <samp>Directory Server:</samp>.

There can only be **one** Server configured for the Autocomplete role, You might have to ask your LDAP Server admin to merge some address books if you have the need to configure multiple of these.

<b>Outdated information:</b> Some places on the internet suggest setting `ldap_2.servers.<addressbook>.enable_autocomplete` to `true` for configuring multiple Servers as autocomplete providers. This no longer works.

<b>If you want multiple addressbooks:</b> Set up an OpenLDAP Server and merge the addressbooks using its `meta` backend. See my [Today I learned OpenLDAP](@content/blog/2025-02-17_today_i_learned_openldap.md) post, if setting up OpenLDAP seems daunting.

---

As one may have been able to guess: I was stuck on that part for quite bit as the LDAP-Server with the S/MIME Certificates was a secondary Address book and therefore not configures as the autocompleting one.

How did I find out? If a general web search isn't helping, look at the bug tracker! Search for LDAP and S/MIME and apparently someone who registered under the name kevin not only reported that they had the same issue, but also [found and wrote down the cause of the issue](https://bugzilla.mozilla.org/show_bug.cgi?id=1121450#c3)! Huge Thank You!

## I hope that was useful

Tanks for reading, I hope that piece of information saved you some time and avoided a headache.

---

You know what also avoids headaches?

Democracy and Human Rights: They allow people to worry about unimportant things like LDAP Address books instead of being on the wrong side of some crapheads mood that might ruin ones live.

**Vote for keeping Democracy and Human Rights!**
