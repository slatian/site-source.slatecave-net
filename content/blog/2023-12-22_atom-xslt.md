+++
	title="Pretty Atom-Feed Previews with XSLT"
	description="Adding some XSLT to my Atom-Feeds to make them behave like Webpages in Browsers"
	date=2023-12-22
	[taxonomies]
	topics=["Web"]
	lang=["XML","HTML"]
+++

## Atom and XSLT what?

I'll assume you already know what [Atom-Feeds](https://en.wikipedia.org/wiki/Atom_(web_standard)) and XML are and that you have some HTML skills.

As an Atom-Feed is simply an XML-File on the web-server it makes sense [linking to it](/about/feeds), unfortunately the Atom-XML in the Browser isn't pretty to look at.

This is where [XSLT (Extensible Stylesheet Language Transformation)](https://en.wikipedia.org/wiki/XSLT) steps in. XSLT is a standardised way to turn an XML document into another and, as it turns out it is [built into every major Web-Browser](https://www.w3schools.com/xml/xsl_transformation.asp), meaning it works perfectly well with my static site setup.

This way one can deliver a standards compliant Atom-Feed that gets rendered into a preview for humans when viewed with a Browser.

Awesome, lets do that!

<b>Note:</b> If you've ever seen the message <q>This XML file does not appear to have any style information associated with it. The document tree is shown below.</q> the Browser means XSLT, not CSS.

## Building an Atom-Feed with a Stylesheet

To get the Browser to load a stylesheet one has to add tell it where it is by adding a line like following:

```xml
<!-- <?xml … header here -->
<?xml-stylesheet type="text/xsl" href="/assets/site_slatecave/atom_to_html.xslt"?>
<!-- Rest of feed here -->
```

For the slatecave I've added the line to [my atom feed template](https://codeberg.org/slatian/site-source.slatecave-net/src/commit/a7ffd694630a425bec04c35b09adcd7461dc9929/templates/atom.xml#L2) and put a very minimalistic stylesheet into the static files.

<b>Note:</b> I didn't have this template before this project so I customised [Zola's builtin template](https://github.com/getzola/zola/blob/38199c125501e9ff0e700e96adaca72cc3f25d2b/components/templates/src/builtins/atom.xml) a little.

<b>Note:</b> If you want to use XSLT 2.0 functions (mainly `replace()` and `tokenize()`) you have to set `version` to `2.0` on the `xsl:stylesheet` tag.

{% figure(caption="A very minimal XSLT-Sheet that results in a hello-world message.") %}
```xml
<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" encoding="utf-8" indent="yes" />
<xsl:template match="/">
<html lang="en">
	<body>
		<p>Hello from the XSLT-Sheet!</p>
	</body>
</html>
</xsl:template>

</xsl:stylesheet>
```
{% end %}

## Getting Content from the Atom-Feed

To get content from the original XML one can use an `<xsl:value-of select="/xpath/goes/here" />` tag.

This tag replaces itself with the content of the first tag returned by the XPath in the `select` attribute.

!note: <b>What is XPath?</b> XPath is a way to select tags from an XML Document, it works a bit like a Filepath and a bit like a CSS Selector.

{% linkbutton(href="/notebook/xpath", icon="destination-text") %}See my XPath Cheatsheet.{% end %}

To spare you the headache: Don't forget to declare and use the `atom` namespace on the XSLT-Sheet.

{% figure(caption="The new opening tag of the XSLT-Sheet.")%}
```xml
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:atom="http://www.w3.org/2005/Atom">
```
{% end %}

Nodes in the Atom feed can then be accessed like the following:
```xml
<xsl:value-of select="/atom:feed/atom:title" />
```

With that out of the way you follow the the [w3schools XSLT Tutorial](https://www.w3schools.com/xml/xsl_intro.asp) to create such a sheet yourself.

{% linkbutton(href="https://codeberg.org/slatian/site-source.slatecave-net/src/commit/a7ffd694630a425bec04c35b09adcd7461dc9929/static/assets/site_slatecave/atom_to_html.xslt", icon="destination-code") %}The slatecave.net Atom to HTML XSLT-Sheet{% end %}

## Content Security Policy

After Uploading I noticed that it isn't working. A quick look at the debugging tools reveals that an XSLT sheet is treated like a script which was disabled, because the slatecave doesn't use any JavaScript.

Setting `script-src 'self';` allows loading and executing the XSLT-Sheet resulting in some [pretty Atom-Feeds](/atom.xml).

## That's it!

I hope that information was useful or at least interesting to you!

This is more a braindump style of post, let me know how that went.
