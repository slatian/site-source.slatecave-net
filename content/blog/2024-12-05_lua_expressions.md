+++
	title = "How Lua compiles expressions to bytecode"
	description = "A little dive into the Lua 5.4.7 compiler to discover how it works."
	date = 2024-12-05
	draft = true
+++

## Lua Compiler?

[Lua](https://lua.org) is an Interpreted scripting language that is mainly made for being embedded into another programs, but also comes with a standalone `lua` executable to run lua scripts outside of any other program.

The Lua interpreter works by first compiling a program to bytecode for a virtual machine and then running that code in said virtual machine.

{% chat() %}
: /resources/emoji/slatian_bean/slatian_bean_happy.png ::: Slatian happily explains
> `<opinion>`Lua is also quite a beatiful scripting language, reason for that being that is follows a mechanism over policy approach which makes very flexible while it is very lightweight and easy to remember all the features with a humane learning curve. `</opinion>`

> Which is why I decided to take a closer look at it to understand how it works under the hood. There is suprisingly little literature out there on how the Lua bytecode works or how it is derived from the source code.

{% end %}

## Prior Art

There are some resources worth reading to understand Lua:

{% linkbutton(href="https://adrian.geek.nz/gnu_docs/scripting-langs.html#lua") %}Adrian Cochrane on Scripting Languages / Lua{% end %}

Provides context on how the rest of lua works on a higher level and is definitely worth reading before the rest of this post.

{% linkbutton(href="https://craftinginterpreters.com") %}Crafing Interpreters, a Book on how to make a scripting language{% end %}

Where Robert Nystrom goes over the basics of scriptign languages and equips everyone who is willing to read for a few days with enough knowledge to build their own little scripting language. (The web version is the full book for free, Tank You!)

{% linkbutton(href="https://www.lua.org/doc/jucs05.pdf", icon="destination-text") %}The Implementation of Lua 5.0{% end %}

A Paper in which Roberto Ierusalimschy, Luiz Henrique de Figueiredo and Waldemar Celes describe the internals of Lua 5.0, mainly focusing on the virual machine and the backing data. It explains most concepts that are important around the compiler, but unfortunately not what the compiler itself does.

{% linkbutton(href="https://www.lua.org/manual/5.4/manual.html#9") %}The Complete Syntax of Lua{% end %}

At the end of each Lua Reference Manual the Lua syntax is attached in [EBNF Notation](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_form). The code of the complier often follows that notation very closely, which makes it easier to understand.

<b>Warning:</b> You should already know Lua aad have some basic knowlege on how a real CPU or bytecode based scripting language works, otherwise you'll not enjoy reading the following blogpost.

## What makes Lua difficult to compile?

Lua bytecode, in this case for the 5.4 virtual machine has a few features that make it more difficult to compile than a simple stack based langugage that only has to convert every statement to [Reverse Polish notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation), arguments first, then the operation. It also has a few opimizations that further complicate the compiler.

Most of these are emplained in [The Implementation of Lua 5.0](https://www.lua.org/doc/jucs05.pdf) in detail.

### Being Register based

As of 5.4 Lua is register based, that means, that the instructions for the VM contain explicit addresses, which means that data doesn't have to be moved around a stack, but can be addressed directly.

Unfortunately this also means that one has to keep track of up to 255 registers.

Spoiler, the Lua developers came up with way to stack the registers in the order they are needed in the compiler, so that keeping track of which registers are free is as simple as incrementing and decrementing a counter.

### Modular conditional jumps

Lua does conditional jumping (i.e. for if statements) by having test instructions, that if a spcific condition is met skip the next instruction for the "continue" or "go through" path and execute the next instruction, which is always a jump to somewhere else otherwise.

While not a huge complication, this requires converting bytecode offsets in a few places one has to be aware of when trying to understand what the compiler does.

### Optimizing `and` and `or`

In lua `and` and `or` aren't just boolean operators, they also can deal with any other datatype and are guranteed to only evaluate the second argument if it is needed.

{% figure(caption="Some examples of how <code>and</code> and <code>or</code> behave in Lua") %}
```lua
false or "fallback" --> "something"
"if this" and "then that" --> "the that"
false or nil --> nil
false and nil --> false
nil and false --> nil
"something" or error("must not be false or nil") --> "something"
nil or error("must not be false or nil") --> throws the given error

```
{% end %}

That is quite some funtionality for so few letters that are used quite often. Lua has an optimized `TESTSET` instruction in its bytecode that, after testing for the truthiness of a value skips one instruction (continue), or sets a given register to the value in the register that was tested against and allows the next instruction (jump) to execute.

This value setting, while it intuitively makes sense that it's an optimization causes a few special cases one should be aware of.

## What is an Expression?

For the purpose of not blowing the article up with complexity, an expression

* is either
	* a simple expression representing a constant or variable
	* a unary or binary operation with one or two nested expressions
* returns exactly one value

Being able ot nest two expressions inside a binary operation makes each expression a tree like structure iwth arbitrarly many branches, this will become important for expression descriptions, as es that means that there is the potential for **a lot** of them having to coexist while compiling any single expression.

<b>Note on Expression Lists:</b> Expression lists in Lua represent multiple expressions that write their results to consecutive registers, this is also reused with single expressions that can return multiple values (functions and `...`), they aren't covered further in this article. What is important to know is that some operations for simplicity reasons depend on that consecutiveness or can be optimized if it is given.

The [Syntax definition attached at the end of the Lua Reference](https://www.lua.org/manual/5.4/manual.html#9) has quite a few more cases of what an expression is, but all of these are more complicated versions of what is outlined here.

## Bytecode Generating and Patching

Bytecode for Lus is generated in multiple stages, at fist for the logic bytecode is emitted. This bytecode exactly decribes what should happen in the pprogram, up to the point that the compiler is currently working on.

However, some details are filled in later by patching the bytecode once those details are known:

{% dl(compact=false) %}
Jump adresses
: If a codeblock needs to be skipped the destination address is known when the compiler is done generating it, at which point the compiler can patch the jump that was needed at the end of the block.
Destination registers
: When the last instruction of n expression is generated the compiler usually doesn't know, where the data will be needed next, so it fills that in once the location the data is needed in is known (see the nor on expression lists why that is useful in a register vm)
Test conditions
: Instead of wasting the intructions of inverting the outcome of some test instruction, Lua simply patches the test instruction to produce the needed reverse outcome.
{% end %}

## Expression Descriptions and the 1.5-pass Compiler

To have a better chance at appling optimizations and to keep track of the current state of the bytecode, like what has to be generated and where patches are needed Lua uses a kind of 1.5 pass compiler where expressions are first compiled to an ExpressionDescription (`ExpDesc`) data-structure which is then passed around and turned into bytecode as needed.

ExpressionDescriptions only store the outcome of an expression, any nested expressions are turned into bytecode to keep the description simple.

Descriptions are things like:
* Direct numeric, boolean or nil Constants
* Constants in the constant table
* The register the outcome of an expression will be stored in (<i>non-relocateable</i>, generated by the `discharge` functions)
* The address that can be patched once the destination for the needed outcome is known (<i>relocateable</i>)
* The address of a test and jump (pointing to the jump) combination to patch for the destination address, test condidion or output register (`TESTSET`).
* Different kinds of accessing variables (not further covered in this post)

Most expressions are first converted to a <i>relocateable</i> expression (this step usually generates most or all of the bytecode) and finally, once the result is needed to a <i>non-relocateable</i> expression.

### Jump Lists

Expression Descriptions also have two lists of jump instructions attached to them, one for a "jump on true" (<i>truejump</i>) and one for a "jump on false" (<i>falsejump</i>), these are used across all types of expressions to carry along the addresses of unpatched jump instructions (which are generated fom test operations, i.e. comparisons or `and` and `or` operators) so that they can be patched once they are known.

At the end of an expression, if it is expected to produce a value the falsejumps are patched to a `LOADFALSESKIP` instruction which loads a `false` into a given register. The truejumps are patched to a `LOADTRUE` instruction.

<b>Note:</b> Wheter the `LOADFALSESKIP` or `LOADTRUE` is/are needed is determined by the `need_value()` function. It is used to test if one of the jumplists contains a non `TESTSET` test instruction.

All jumps that bring their own value (jumps controlled by a `TESTSET` instruction) are patched load into the same register as the boolean loading instructions and the jump destination set to after the boolean loaders.

{% figure(caption="Illustration of what the generated bytecode looks like.") %}
```
# Code arrives here with a value in [OUT] if it hasn't taken a truejump or falsejump path.
	JUMP end
falsejump:
	LOADFALSESKIP [OUT] # Skips the LOADTRUE instruction
truejump:
	LOADTRUE [OUT]
testset:
end:
# Code continues here, with a guaranteed value in out.
```
{% end %}

The patching happens when the expression gets converted to a non-relocateable to expression because its values needed in a register for further processing.

!warning: <b>TODO:</b> Add which functoins in the Lua source are responsible for this.

### Discharging

The expressions get altered to represent how much of and how far the experession is compiled.

This process is called discharging.
