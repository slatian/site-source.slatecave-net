+++
	title="Idea: An UI for small character LCDs"
	description="An Idea for a remote UI that runs on character LCDs with 4 or 5 buttons of input."
	date="2023-05-26"
	draft=true
+++

## The Why

I've already built an alarm clock based on what effectively is an Arduino Uno with the hardware for the User (me) facing part being a 16x2 character LCD and a little 5 button joystick (up, down, left, right, ok) and it's working pretty well. The UI, while being pretty big comfortably fits into the ATmega328P and is lightning fast.

## Input

The input with the joystick is pretty well suited for a menu system.

For simplicity I'll call the left and right buttons <kbd>forward</kbd> and <kbd>backward</kbd>.

In menu mode:
* <kbd>up</kbd> and <kbd>down</kbd> are for navigation inside a menu
* <kbd>backward</kbd> brings one back to the previous menu
* <kbd>forward</kbd> or <kbd>ok</kbd> means activate the menu entry, resulting in navigating to a submenu, an editor or toggling a switch.

In editor mode:
* <kbd>forward</kbd> and <kbd>backward</kbd> move a cursor
* <kbd>up</kbd> and <kbd>down</kbd> are for manipulating whatever the cursor is on
* <kbd>ok</kbd> is for accepting whatever was entered as the current new value.
* Pressing and holding <kbd>backward</kbd> translates to a cancel action.

Pressing and holding the <kbd>forward</kbd> button could be mapped to replace the <kbd>ok</kbd>. This can either be done because the hardware only allows for 4 buttons in a future scenario or because the <kbd>ok</kbd> button is sometimes a bit difficult to press without also activating a directional input.

<b>Note:</b> I currently abuse that to make sure I can't confuse snooze and acknowlege actions when the alarm is trying to wake me up.

## Display

The Display being a 16x2 character LCD means we have some interesting limitations.

* We only have 2 lines of text 16 characters wide each.
* Just plain monopaced text
* Only support for ASCII characters

While also having some interesting features:

* There is a builtin cursor that can either be just a line or blinking.
* Up to 8 custom characters that fit into a 5x8 pixel grid.

### Making use of it

To make use of the display I decided on reserving the first line for a title. This mostly is to avoid getting lost and to communicate the current state of the object the current view belongs to (i.e. which time and days of a week the alarm that one is currently editing will ring).

The second line remains for displaying the currently selected menu item, an editor or a message.

When the text is too long it should scroll. Too long text should however be avoided for the title as scrolling that one would mean contant movement on th screen which is distracting.
