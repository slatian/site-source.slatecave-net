+++
	title = "Today I learned: OpenLDAP"
	description = "Things I wish I knew before starting with OpenLDAP."

	[taxonomies]
	topics = ["Linux Administration", "LDAP"]
+++


## Who is this for?

This is for people who know how to configure their LDAP clients, but now want some knowledge on how to configure a server.

{% chat() %}
: /resources/emoji/slatian_bean/slatian_bean.png ::: Quick not from Slatian
> This is actually a time delayed "Today" as finding all the links again took a bit.

> And don't worry, configuring LDAP isn't **that** hard, most if it is because of difficult to find documentation and a missing birds eye view.
{% end %}


## How to configure

While the developers of OpenLDAP are really pushing their online configuration (olc) the textfile based legacy configuration is still fully functional and more readable as well as accessible for testing (Especially if this is your fist own LDAP server).

Note, that whatever whenever you are configuring a database or overlay, you probably have to load the corresponding modules (see your distributions example configuration for where exactly the modules are, usually somewhere around `/usr/lib/ldap/` or a similar path.) The backend modules (database types) are all called `back_<type>`.

As for documentation: The OpenLDAP Admin Guide is your friend here:

{% linkbutton(icon="action-next", href="https://www.openldap.org/doc/") %}OpenLDAP Documentation{% end %}

If you can't find what you are looking for there, see if there is a `slapd-` or `slapo-` manpage on the module you need help with.

The Samba Wiki contains some useful configuration templates if your goal is to build a some kind of proxy or mirror Server:

{% linklist(icon="action-next") %}
* [slapd.conf Slave syncrepl OpenLDAP 2.2](https://wiki.samba.org/index.php/2.2.1_slapd.conf_Slave_syncrepl_Openldap2.2)
* [OpenLDAP as proxy to AD](https://wiki.samba.org/index.php/OpenLDAP_as_proxy_to_AD)
{% end %}

<b>Note:</b> OpenLDAP doesn't really care what it is proxying, so while those example might be for a SAMBA AD, they work for something like an addressbook too.

If you have any problems with your configuration set the debug/log level to `64` (log configuration errors) or `-1` (all errors, warnings, debug and tracing, very verbose.)

See the admin guide for a full list of log levels.

!note: <b>Log Level Hint:</b> The log levels double, because that is a bitfield, one can combine them with a binary or. That's why `-1` (the binary representation of -1 has all bits set) turns on all log messages.

## Proxy helpers

If you want to proxy or synchronise from another server you want to use the `ldap` backend with an optional [`pcache` (proxy cache)](https://www.openldap.org/doc/admin26/overlays.html#The%20Proxy%20Cache%20Engine) overlay or an [`mdb` (older configurations use its predecessor `bdb`) backend](https://www.openldap.org/doc/admin26/slapdconfig.html#MDB%20Database%20Directives) with the `syncrepl` directive.

Rewriting the requests from one suffix to another search for `massage`. If the module you want to use doesn't support `massage`,  (i.e. serving from an `ldap` backend) there is the [`rwm-suffixmassage` directive](https://manned.org/man/slapo-rwm#head5) that can be configured on an `rwm` overlay.

```
loadmodule rwm

[… regular db config goes here …]
overlay rwm
rwm-suffixmassage ou=new,dc=suffix,dc=to,dc=export,dc=as
```

Rewriting may be necessary to support legacy systems or to "mount" a subtree under a different name.

For combining multiple backends there is the [`meta` backend](https://manned.org/man/slapd-meta), that can combine requests from multiple `uri` directives (the suffix to mount as is the URL path), the meta backend also supports a `suffixmassage` directive that can be placed after each `uri` directive.

<b>Note on `meta` and `suffixmassage`:</b> The path in the URL is the suffix that the backend should be served under, not the path on the backend! This confused me the first time because it combines to seemingly unrelated settings in one configuration value, but it makes sense when seen without the `suffixmassage` directive

Chaining up multiple, more complex configurations into a `meta` backend is easiest when you define the individual sub-LDAP directories as if they weren't part of the `meta` backend under a dummy suffix and then letting OpenLDAP access itself via the `ldapi:///` scheme using Unix sockets (Unix sockets to avoid involving the network stack, faster).

Access rules have to be redefined on the `meta` backend as it can't communicate who is actually accessing the frontend, for the backends the requests are coming from the LDAP server.

## Manually running `slapd`

Manually running `slapd` can be useful for testing or building containers.

```sh
slapd -h "ldapi:// ldap://" -f /path/to/slapd.conf -d 64
```

Where:

{% dl() %}
`-h`
: Space separated list of active sockets as URIs

`-f`
: path to ("legacy") configuration file

`-d`
: Debug/Log level
{% end %}

{% linkbutton(icon="destination-text", href="https://manned.org/man/slapd") %}man slapd{% end %}
