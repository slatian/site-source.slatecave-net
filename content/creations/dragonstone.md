+++
	title = "Dragonstone - Gemini/Gopher Browser"
	description = "Dragonstone is a browser for the Gopher and Gemini protocols built with Gtk 3."
	date = "2019-11-03"
	[extra]
	creation_status = "Usable, rewrite planned"
	sourcecode_uri = "https://codeberg.org/slatian/dragonstone"
	[taxonomies]
	lang=["Vala"]
+++

## Overview

It started as a Gopher browser inspired by the Pocket Gopher Android App, later support for the gemini and finger protocols was added. It also has theming support for documents. I use it as my primary gopher/gemini browser.

## A little background and history

### So why does someone write a browser at all?

The project started as a coincidence: At the end of 2019 I lwaenewd about a protocol called gopher and started exploring some gopherholes … On an Android Phone with an app called Pocket Gopher which looked nice, was useable but had no cache at all and I wanted a desktop app anyway. At the same time I found a [Vala Tutorial by Alessandro Castellani](https://youtube.com/playlist?list=PLriKzYyLb28mn2lS3c5yqMHgLREi7kR9-) and needed a project to turn all the things I've learned into something useful …

While the original plan was to write a gopher browser with local file browsing capabilities I discovered the [Gemini protocol](https://gemini.circumlunar.space/), and wrote [some extra code to handle gemini requests](https://codeberg.org/slatian/dragonstone/commit/b66b58e23f6d9962719507355ecf007f227f7505).

Fun fact: A few days later Julien Blanchard started working on the [Castor browser](https://sr.ht/~julienxx/Castor/), where I later copied the idea to use a GtkTextView for document rendering (dragonstones terrible link implementation was my idea to get rid of some performance issues with the button in textview approach).

### Request infrastructure

<b>Or: Why making a browser is easy but hard to get right</b>

The request infrastructure of Dragonstone was originally based on the idea that a request with an uri would be passed tp a resource store, processed and then be returned to the tab wich selects a rendering widget based on the success and file type. (simple gopher browser with the capability to view local files)

Letting the widgets see (and touch, uploading in dragonstone is a giant hack) the requests was a GIGANTIC mistake, UI code should NEVER do any application logic …

It also turned out (earlier actually) that thinking in therms of resource stores was also a flawed Idea, caching is required across almost all networking protocols and sometimes a resource store needs extra information like a certificate, the "session" was born a rewrite of a lot of code later to have this functionality in seperate blocks.

### Failed rewrites

There were two attempts to rewrite Dragonstone:

* [The Nightcat Experiment](https://codeberg.org/slatian/browser-experiment.nightcat)
* [The Slate Experiment](https://codeberg.org/slatian/browser-experiment.slate)

(Note that I'll reuse both names and I'll refer to them as experiments and eventually update the git repo. You probybly have noticed, that I reused Slate for this website and my new nick)

Nightcat was an excellent Request framework, except that I realized too late that When streaming resurces there could be a result before the final result and that killed it because it was built with the assumption that the browser waits for the result with only a progress indicator.

Slate on literally the other end reused Dragonstones flawed request framework in an attempt to decouple the UI from the requests entirely before eventually replacing the requests framework with a better one. It even reused Dragonstones document renderer, unfortunately this approach takes time and I put it on hold in favour of some other project. (It is unuseable in its current state, but maybe I'll pick it back up some day)
