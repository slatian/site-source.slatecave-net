+++
	title = "A simple TicTacToe game"
	description = "Written for the commandline this is my first game I've written myself, still fun to have around in 2020."
	date = "2018-07-07"
	[extra]
	creation_status = "Completed"
	sourcecode_uri = "https://codeberg.org/slatian/game.tic-tac-toe"
	[taxonomies]
	lang=["Lua"]
+++

## Overview

## How to play

<figure>
<figcaption>A sceenshot of a game currently in progress, playing field rendered in ascii-art at the top, a little guide which input number corresponds with wich field and a prompt asking O for their move.</figcaption>
<pre><samp>
                 ### ##\          /## ###                 
                 ###  \##\      /##/  ###                 
                 ###    \##\  /##/    ###                 
                 ###      \####/      ###                 
                 ###      /####\      ###                 
                 ###    /##/  \##\    ###                 
                 ###  /##/      \##\  ###                 
                 ### ##/          \## ###                 
##########################################################
##########################################################
                 ###                  ### ##\          /##
                 ###                  ###  \##\      /##/ 
                 ###                  ###    \##\  /##/   
                 ###                  ###      \####/     
                 ###                  ###      /####\     
                 ###                  ###    /##/  \##\   
                 ###                  ###  /##/      \##\ 
                 ###                  ### ##/          \##
##########################################################
##########################################################
##\          /## ###    __________    ###    __________   
 \##\      /##/  ###   / ________ \   ###   / ________ \  
   \##\  /##/    ###  / /        \ \  ###  / /        \ \ 
     \####/      ### | |          | | ### | |          | |
     /####\      ### | |          | | ### | |          | |
   /##/  \##\    ### | |          | | ### | |          | |
 /##/      \##\  ###  \ \________/ /  ###  \ \________/ / 
##/          \## ###   \__________/   ###   \__________/  
789
456
123
O's turn:
</samp></pre>
</figure>

The game is pretty simple, X starts and then X and O make their moves by entering the number that corresponds to the field they want to place their marker on, if one of then gets a row of three they win, if the field fills up dbefore that happens the game is a draw.

Hint: Your Numpad probably has the same layout as the field numbers!
