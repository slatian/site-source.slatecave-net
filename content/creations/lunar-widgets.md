+++
	title = "Lunar Widgets - Lua scriptable Gtk Widgets"
	description = "Lua scripting for Gtk without gobject introspection, similar to eww, useful for prototyping"
	date = "2021-07-29"
	[extra]
	creation_status = "Unfinished, experimental"
	sourcecode_uri = "https://codeberg.org/slatian/lunar-widgets"
	[taxonomies]
	lang=["Vala"]
+++

## Overview

This started out as a simple program to show custom keyboards (the test.lua example is a custom keyboard for newsboat) that could be useful on a touchscreen but is now going in the general direction of [Eww](https://github.com/elkowar/eww) (This project is far away from what Eww can do).

The API is a bit more powerful than documented in the README, it's probably best to look at the examples and the sourcecode to find out what else can be done. I once wrote a prototype for a notification display that actually worked (notifications got fed in via stdin).
