+++
	title = "Swaylock Mobile"
	description = "Sxmo with sway is great but unfortunately it lacks a proper lockscreen, This is my attempt to fix that situation by adding a keypad and some mobile related cosmetic features to swaylock."
	date = "2022-03-03"
	updated = "2023-05-28"
	[extra]
	creation_status = "Abandonned proof of concept"
	sourcecode_uri = "https://codeberg.org/slatian/swaylock-mobile"
	fork_of = "swaylock"
	fork_of_uri = "https://github.com/swaywm/swaylock"
	[taxonomies]
	lang=["C"]
	topics=["Mobile Linux"]
+++

## Overview

So far I have implemented:

* A flat typing indicator that is (in my opinion) a better fit for small screens
* A numeric keypad at the bottom, currently only reacting to touch input that is not very configurable

Also this is my first wayland project and my first attempt at writing more than a "Hello World" in C.

## Log

### 2022-03-06 Initial release

The first goal was to get a working prototype that looks acceptable (Some alignments are still off). I have reached this by implementing a flat indicator that works a bit like the ring but is a line with the width configured using the radius and if necessary with a text-box below.

I was able to lock my phone by manually calling it and unlock it using the new keypad, next step is to integrate it with Sxmo so that I finally don't have to rely on the incompetence of a potential attacker for the security of my phone.

### 2022-03-08 schlock

Apparently I'm not the first to attempt to write a lockscreen with numeric keypad based on swaylock, [schlock](https://github.com/telent/schlock), written by [@telent_net](https://ww.telent.net/) appeared about two weeks ago. (Thank you for the link goes to [Willow Barraco](https://www.willowbarraco.fr/))

It also is targeted at the touchscreen + numeric pin usecase but our approaches under the hood seem to be completely different.

While the overall approach with schlock is to replace the UI of swaylock with a virtual numpad and a custom indicator (see schlock's [render.c](https://github.com/telent/schlock/blob/main/render.c) and [pinentry.c](https://github.com/telent/schlock/blob/main/pinentry.c)) the my swaylock-mobile trys to adapt and expand the UI to include touchscreen functionality so that my fork remains useful on non-touchscreens. However, the outcome is mostly the same for touchscreens.

On the password matching side schlock is a bit more creative than swaylock-mobile, it seems to (I'm not sure, judging based on having looked at the commit diffs for about half an hour) have a separate code-path that matches against the hash of a pin in a file (that could be a backend and it would be awesome!) while mine works like the phosh lockscreen and simply asks pam (or whatever your swaylock backend is) if the entered pin is the users password (ab)using the existing mechanism for keyboards.

There is no better here, just ideas from two screenlockers aimed at a phone that runs linux with their own ideas on how to get from a common starting point to a common goal.

### 2022-03-19 Integrating with sxmo 1.9.0

I wrote a [blogpost about how screenlocking in sxmo works](@/blog/2022-03-18_integrating-my-screenlocker-with-sxmo-part-1.md) pre 1.9.0 and post 1.9.0 and how I'd integrate swaylock-mobile (or any other screenlocker) with it.

### 2023-02-26 Curently abandonned

Because of some real-life happenings and some other projects I declare this as officially abandonned, I currently don't need my phone. That doesn't mean, that I won't pick it up again.

### 2023-05-28 sxmo_swaylock

KaffeinatedKat merged the swaylock-mobile keypad into his [sxmo_swaylock](https://github.com/KaffeinatedKat/sxmo_swaylock) project after I [proposed it to be merged into swaylock-effects](https://github.com/mortie/swaylock-effects/issues/100). Thank you for adopting it!

Yes I'm very okay with someone else adoping the code and/or idea too!
