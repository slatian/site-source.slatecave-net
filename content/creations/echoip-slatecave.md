+++
	title = "echoip.slatecave.net"
	description = "A web-service for looking up IPs and domain names"
	date = "2023-02-26"
	[extra]
	creation_status = "Live and in color!"
	sourcecode_uri = "https://codeberg.org/slatian/service.echoip-slatecave"
	[taxonomies]
	topics=["Web","Tools"]
	lang=["Rust"]
+++

## Overview

{% linkbutton(href="https://echoip.slatecave.net") %}Visit echoip.slatecave.net{% end %}

This service was born out of a combination of wanting to learn rust, not being very happy with [the echoip service by mpolden](https://github.com/mpolden/echoip) which had some features missing that I wanted in my echoip service.

So what echoip-slatecave do?
* Look up IP-Addresses in a mmdb database and return the resulting geoinformation.
* Reverse DNS lookup
* DNS forward lookup for the most common record types, all on one page
* Has a text view that is made for commandline use
* Links to other services that may be useful in the HTML version
* Has a builtin rate-limiter that makes easier to protect a small self hosted instance
* Interlinked IP-Address and domain lookup to make navigating easier

With that feature set it should be a useful tool where one can enter any kind of address and navigate straight to the needed information or start exploring (which works pretty well, when the IP and domain lookup are interlinked).

## Why?
{% chat() %}
: /resources/emoji/neofox/neofox.png ::: Neofox asks:
> But slatian, why? And why include a DNS lookup? I can do this on the commandline!
{% end %}

Besides the fact that exploring is easier with a web interface that does multiple lookup with which is one of the reasons I built it …

Yes YOU can use the commandline and make sense of it, I can too. But not everyone has the skill or system to use a capable commandline, also I don't want to link to some random non-free wannabe monopoly service because if someone didn't have a tool they'll continue using that.
