+++
	title = "Some of my creations"
	description = "I build lots of stuff, some of it is good enough to publish it and tell the story behind it."
	template = "creations-index.html"
	page_template = "article.html"
	sort_by = "update_date"
	[extra]
	short_title = "Creations"
+++

## Other places you can find my projects

<ul class="link-list">
<li><a href="https://codeberg.org/slatian/" class="decoration-destination-listing">codeberg.org/slatian</a></li>
<li><a href="https://gitlab.com/baschdel/" class="decoration-destination-listing">gitlab.com/baschdel (legacy)</a></li>
</ul>
