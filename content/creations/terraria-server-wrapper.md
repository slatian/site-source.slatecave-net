+++
	title = "A wrapper for a terraria server with some perks"
	description = "Adds automatic shutdown when not needed, Matterbridge support and exposes Server commands to the ingame chat."
	date = "2021-05-05"
	[extra]
	creation_status = "Completed"
	sourcecode_uri = "https://codeberg.org/slatian/terraria-server-wrapper"
	[taxonomies]
	lang=["Python"]
+++

## Overview

## How it came into existence

Long story short: A friend convinced me and a few other friends to try out [Terraria](https://terraria.org/), we played together using his PC as the server and immedately ran into a strange problem related to us being in germany and demanding bandwidth from our ISPs.

Setting up a server on a cloudserver wasn't a big problem, but I wnated something that shut the Server down when nobody used it while we were in School. So I wrote a few lines of python … and called that thing `terraria-supervisor.py`.

The other features were a: <q>Wouldn't it be cool if?</q>

First we got annoyed by the time it took to get dark (or become day again), and I wasn't very keen on logging into my Server every time so I added the cheaty part which redirected the server output to the ingame chat and in reverse read the ingame chat and looked for something command like and pipes that into the admin console using a pcall.

The chatty part was me already having a Matterbridge set up for our little community (because I don't like Discord) and on the other end having a script that can read and write our ingame chat.

## A few tips on Game Server administration

Unfortunately that server didn't last very long until it was abandonned, here is the lesson I learned:

Disable cheating and bringing in stuff from the outside (<em>and write that rule along with the reasoning down somewhere!</em>), someone will eventually join with their level 9001 character or turn the cheatclient on, you all have a couple of hours of fun beat that one boss in no time at all and then be greeted with a gameworld that is too difficult for everyone playing fair (and boring for the others), a rollback feels like loosing progress, both options kill the world on the server and the players (including YOU, the admin) won't come back.
