+++
	title = "A Tetris clone"
	description = "A simple Tetris game written in Python that started as a scool exercise."
	date = "2019-05-21"
	[extra]
	creation_status = "Completed but messy code"
	sourcecode_uri = "https://codeberg.org/slatian/game.tetris"
	banner = "tetris"
	[taxonomies]
	lang=["Python"]
+++

## Overview

It uses the [cs1graphics library](https://cs1graphics.org/) for painting and is the project I learned python with.

Information about gow to play and a little overview for Modding the game is in the README.

## A bit about the Engine

The engine is split into two parts:

* The renderer which gets an array of squares the preview, score and some text for an optional overlay and draws this into a window.
* The other part is the game mechanics part which implements the game logic.

The tile rotations are calculated on the fly from an array of relative square positions, so if you want to try out some crazy shapes go ahed (The function is a generator so in theory one could also generate crazy tiles on the fly)

Because of the split theming (alias changing the colors) is also pretty simple. Also I remember the black-white version looking rad!
