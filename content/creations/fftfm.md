+++
	title = "FFTFM"
	description = "A fuzzyfinder script that can search for manpages and RFCs"
	date = "2022-12-11"
	[extra]
	creation_status = "Useable"
	sourcecode_uri = "https://codeberg.org/slatian/fftfm"
	[taxonomies]
	lang=["Bash"]
	topics=["Tools"]
+++

## Overview

This script uses [`fzf`](https://github.com/junegunn/fzf) hooks to make it possible to search with "live" result preview for manpages or RFCs, it also contains some experimental special syntax logic which is supposed to be used for filtering sections of manuals but that part doesn't work very well. It also has issues sorting a particualr manpage to the top when an exact name in typed in (probably due to how `apropos` works)

Note that for the RFCs to work it assumes they are in the place and structure the archlinux [`rfc`](https://archlinux.org/packages/community/any/rfc/) package places them.


