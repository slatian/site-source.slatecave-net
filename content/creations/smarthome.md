+++
	title = "My current Smarthome Setup"
	description = "A raspberry pi under my desk that switches screens and audio on and off. commandline edition!"
	date = "2021-08-24"
	updated = "2022-04-12"
	[extra]
	creation_status = "Actively used"
	sourcecode_uri = "https://codeberg.org/slatian/smarthome"
	[taxonomies]
	lang=["Lua","Sh"]
+++

## Overview

### Features

* Can turn GPIOs on and off (I use that for routing audio and switching relays for power outlets)
* Pings my machines every few seconds to find out if they are on (I have planned for when that assumption falls flat)
* Gemini based API (for keyboard shortcuts) and dashboard
* Scriptable using anything that can read from and write to stdio (I use lua)

### Documentation

{% linkbutton(href="https://codeberg.org/slatian/smarthome", icon="action-next")%}Detailed documentation is in the README{% end %}
