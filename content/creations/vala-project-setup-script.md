+++
	title = "Vala project setup script"
	description = "This project is a lua script that automates the process of setting up a project with vala, meson and git. Similar to what happens when yo tell cargo or go to create a new project. It also sets up some helper scripts and a README with build instructions."
	date = "2021-05-27"
	[extra]
	creation_status = "Completed"
	sourcecode_uri = "https://codeberg.org/slatian/vala-project-setup-script"
	[taxonomies]
	lang=["Lua","Vala"]
+++

## Overview

When experimenting with gtk and Vala I notices that I was copy-pasting files to bootstrap new projects and experiments all the time, replacing names by hand and … making mistakes. So I put everything into a lua script that helps me with bootstrapping Vala based projects (which at the time of writing, mid 2022 I still do on occasion)

## Features

### Automatic generation of meson.build files

The script generates two buildfiles for meson, one in the projects root where meson finds it and a second one in the src folder that contains a list of all the files and your dependency list you probably want to extend.

It also gives you a script that you can call to update the list of files to be compiled (list of `*.vala` files in the src directory) which is good enough for most projects.

### One command to build and run

Building with meson is a multi command process, you have to tell meson to generate the build directory then you have to tell ninja to do the actual building, find the result and run it … too many braincycles wasted for my taste, this script gives you a `build` and a `run` script that can do all the work for you (the run script invoking the buildscript on every call)

Since you are in charge as soon as the script exits you can do whatever the hell you want with those.

### Generates an initial README.md wich explains how to use the build-system

Probably most often asked question by everyone: <q>How do I build that thing?</q>

This thing generates a README.md skeleton for you. (you still have to fill it with information, but at least you don't have to copypasta your explanation of how to use meson)

### Optionally generates a hello world program

Sometimes its nice to have a bit of code that reminds one of the Language syntax … , this also helps with testing the setup script itself.

### Sets up a git repository and makes the initial commit

Another two commands that you type in everytime and are everytime the same commands, so while I was at it I automated it.

## Example invocation

<figure>
<samp>
<code>$ <kbd>setup_project.lua</kbd></code>
New Vala Project<br>
This program will setup a new vala project using meson<br>
-------------------------------------------------------<br>
Please enter the name of your project<br>
&gt; <kbd>slate</kbd><br>
Please enter the reverse domain name of the projects home<br>
&gt; <kbd>net.slatecave</kbd><br>
Create a main.vala file? (y/N)<br>
&gt; <kbd>y</kbd><br>
rename.sh is a script that is able to run a single sed command across every *.vala file in the src/ directory replacing anything with everything, wich is useful for renaming classes and interfaces but can also do a very excellent job at ruining your day without asking if you screw up and don't have a backup!<br>
-- If you enable it make sure you have a versioning system ready!! --<br>
Add rename.sh? (Use a versioning/backup system!) (y/N)<br>
&gt; <kbd>y</kbd><br>
Initialize git repository? (y/N)<br>
&gt; <kbd>y</kbd><br>
Add a remote git repository? (y/N)<br>
&gt; <kbd>y</kbd><br>
Ok, create your remte repo and give me the (clone) address.<br>
&gt; <kbd>https://example.org/git/slate</kbd><br>
-------------------------------------------------------<br>
Project Executable Name: slate<br>
Project id: net.slatecave.slate<br>
Local project folder: /home/baschdel/Development/site_generator/src/projects/vala-project-setup-script<br>
+ Add a main.vala file<br>
+ Add the rename.sh bulksed script<br>
+ Initialize a git repository with remote https://example.org/git<br>
Is this information correct? (y/N)<br>
&gt; <kbd>N</kbd><br>
Please restart the script then.<br>
</samp>
</figure>

I hope this helps people understand the code (you shouldn't let code that some random wierdo from the Internet wrote manage your projects without making sure that code does what you want).
