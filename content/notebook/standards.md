+++
	title="Standards Zoo"
	description="Bookmarks for public content of widely used ISO-Standards."
	date=2024-06-01
	[taxonomies]
	notebook=["Bookmarks"]
+++

## Why this page exists

Many things in our world interoperate because they are standardised. Most notably the internet which is built on [freely available Standards published by the Internet Engineering Task Force (IETF) on rfc-editor.org](https://rfc-editor.org).

However most standardisation organisations charge a premium for the standard documents. Through various legal frameworks and publishing by the organisations in their own interest the important content of those standards is public knowledge.

!note: <b>Useful Previews:</b> ISO has free previews of specifications that only include the informative sections. (Those may already contain the information you need to understand something.

This page aims to collect these in one place, one Section per topic.

## Timekeeping

For Date and Timestamps there are [ISO 8601-1](https://www.iso.org/standard/70907.html) and [ISO 8601-2](https://www.iso.org/standard/70908.html). The most important part of those (exact timestamps) is also known as [RFC 3339](https://www.rfc-editor.org/rfc/rfc3339) which got extended by [RFC 9557](https://www.rfc-editor.org/rfc/rfc9557).

The [Wikipedia article on ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) also explains pretty well what the standard specifies.

## Languages

### Language Identification

When in need of a list of codes for unambiguous language identification the standard is [ISO 639](https://www.iso.org/standard/74575.html) ([ISO 639 Popular standards article](https://www.iso.org/iso-639-language-code)).

One of the official language registries is maintained by the US Library of Congress. A [list for languages titled ISO 639-2](https://www.loc.gov/standards/iso639-2/) and one for [language families titled ISO 639-5](https://www.loc.gov/standards/iso639-5/).

The [complete ISO 639-3 language list](https://iso639-3.sil.org/) is officially maintained by SIL International ([SIL also promotes other language related standards](https://www.sil.org/language-technology/standards)).

### Language Tagging

For language tagging (i.e. describing a documents language, like the [HTML `lang` attribute](https://html.spec.whatwg.org/multipage/dom.html#the-lang-and-xml:lang-attributes)) there is the [BCP 47 collection of standards](https://www.rfc-editor.org/info/bcp47) the corresponding [language subtag registry maintained by IANA](https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry) contains codes and descriptions for languages, language variants, regions, scripts. Most of them are compatible with their ISO counterparts.

### Script Identification

For unambiguous script identification [ISO 15924](https://www.iso.org/standard/81905.html) assigns unambiguous four-letter codes to every script in wide enough use that they are relevant. The [ISO 15924 code list](https://www.unicode.org/iso15924/) is maintained by the Unicode consortium.

## Regions and Countries

For identifying countries and their subdivision there is ISO 3166 ([ISO 3166 Popular  standards article](https://www.iso.org/iso-3166-country-codes.html)). The Machine readable lists are a paid service, but there is a [human readable list of the ISO 3166-1 codes](https://www.iso.org/obp/ui/#iso:pub:PUB500001:en).

There is a useful [List of ISO 3166 country codes on Wikipedia](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes).

The [BCP 47 language subtag registry](https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry) also maps some of the Alpha-2 codes to English country names.

### Places with Transportation Infrastructure

For places with Transportation Infrastructure there is [UN/LOCODE](https://unece.org/trade/uncefact/unlocode). It also has a [list of Countries and their subdivisions](https://service.unece.org/trade/locode/2023-1%20SubdivisionCodes.htm) and a [list of Countries and places with transportation infrastructure](https://unece.org/trade/cefact/unlocode-code-list-country-and-territory) relevant for larger scale trading.

## Business

### Identifying Currencies

For unambiguous currency identification [ISO 4217](https://www.iso.org/standard/64758.html) ([ISO 4217 Popular standards article](https://www.iso.org/iso-4217-currency-codes.html)) specifies lists of current and historic currencies. The [currency registry](https://www.six-group.com/en/products-services/financial-information/data-standards.html) is maintained by Six Group Ltd.
