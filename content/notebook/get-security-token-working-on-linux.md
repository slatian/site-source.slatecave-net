+++
	title="Getting your Smartcards and Security Tokens working on Linux"
	description="You just bought a popular Security Token to find it not working out of the box on Linux?"
	date=2023-12-03
	[taxonomies]
	notebook=["Guides"]
	topics=["Security Tokens"]
+++

Most Problems I had with Smartcards or Security Tokens just boiled down to missing packages. (`pcsclite`, `ccid` and `libfido2`)

## What are Smartcards and Security Tokens?

[Security Tokens](https://en.wikipedia.org/wiki/Security_token) are small devices that hold cryptographic keys and implement some number of protocols that make those keys usable without them ever leaving the device.

This behaviour is useful because nobody can steal your key from such a device, even if you plug that Security token into a virus dripping publicly accessible PC (though that doesn't mean you should).

Popular security Tokens are: [YubiKey](https://wiki.archlinux.org/title/YubiKey), [Nitrokey](https://wiki.archlinux.org/title/Nitrokey), the [Tillitis TKey](https://wiki.archlinux.org/title/Tillitis_TKey) and [SoloKey](https://wiki.archlinux.org/title/Solo).

<b>Note:</b> I'm not qualified to to recommend any Security Tokens, these are just the popular ones I know people will search for.

Smartcards are the same technology in card form, examples include modern bank cards, passports and other credit card sized plastic lumps with a chip baked into them.

I'll use the terms <i>smartcards</i> and <i>security token</i> interchangeably here.

## Install missing Packages

For some reason the utilities for using Smartcards or  don't declare the needed backends as their dependencies, those backends are needed to turn sometime obscure USB or NFC interfaces into APIs that applications can access without worrying about device details.

### `pcsclite` and `ccid`

> Middleware to access a smart card using SCard API (PC/SC).

[pcsclite](https://pcsclite.apdu.fr/) provides a service that takes PC/SC hardware and patches it through to userspace. Usually you need a driver for your Smartcards reader which comes in the `ccid` (sometimes `pcsc-ccid`) package. (You may need a more specific driver in some cases)

It is needed for the part of the smartcard that can do actual signing and encryption, and is commonly used with OpenPGP or PKCS#11.

{% linklist() %}
* [The ArchWiki on Smartcards](https://wiki.archlinux.org/title/Smartcards)
* [The Debian Wiki on Smartcards](https://wiki.debian.org/Smartcards)
{% end %}

### `libfido2`

[FIDO2](https://en.wikipedia.org/wiki/FIDO2_Project) (the successor to [Universal 2nd Factor](https://en.wikipedia.org/wiki/Universal_2nd_Factor)) provides authentication functionality usually used as a second factor to log into websites.

Usually installing the `libfido2` package solves the problem that your Security Token is not recognised as it also contains all the needed udev tweaks and so on. Maybe a reboot is necessary.

Testing this functionality with your browser can be done using [Yubicos demo page](https://demo.yubico.com/).

{% linkbutton(href="https://wiki.archlinux.org/title/Universal_2nd_Factor") %}The ArchWiki has more information on Universal 2nd Factor and FIDO{% end %}

!note: <b>YubiKey Note:</b> Installing `libfido2` also fixed the not very helpful <br>`WARNING: No OTP HID backend available. OTP protocols will not function` <br>for the YubiKey Manager (`ykman`).

## Interesting Things to do with a Security Token

Now that your token is working why not try some interesting things?

<b>Warning:</b> Interesting also means that things can go wrong. Make sure you don't lock yourself out of anything important and use a test system if in doubt.

In case you want to use these devices to log into your Linux user or authenticate `sudo` with one of these Tokens you want to look into PAM Modules. See the [ArchWiki on Authentication for User Sessions with FIDO2](https://wiki.archlinux.org/title/Universal_2nd_Factor#Authentication_for_user_sessions) for more information.

You can also secure you ssh-keys with these tokens. See the [ArchWiki on ssh with FIDO/U2F](https://wiki.archlinux.org/title/SSH_keys#FIDO/U2F) or the [Talk <q lang="de">Noch besser leben mit SSH</q> (from 35:18)](https://media.ccc.de/v/gpn21-28-noch-besser-leben-mit-ssh#t=2116) (at the time of writing no English translation is available) held by leyrer at GPN21.
