+++
	title="arduino-cli"
	description="Some arduino-cli common patterns and tricks"
	date="2023-02-13"
	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Electronics and Microcontroller","Shell","Tools"]
+++

While `arduino-cli` is an awesome tool, unfortunately most guides skip right past it and assume you use the Arduino IDE, this page is a collection of common patterns for `arduino-cli`. It assumes that you know a few things about how Arduino works.

## Basics

To create a sketch `arduino-cli` assumes that your main `.ino` file has the same name as the directory containing it.

To build and upload use `arduino-cli compile --fqbn <fqbn>` and `arduino-cli upload -p <serial-device> --fqbn <fqbn>` in your code directory.

!note: <b>Hint:</b> On Linux your serial devices are called `/dev/ttyUSBn` and `/dev/ttyACMn` where `n` is a number.<br> [Make sure you have the permissions to access them and nothing else is trying to use those ports.](@/notebook/serial-not-working.md)

!note: <b>You've got an unhelpful error message?</b> Try adding the `--log` flag, it will cause `arduino-cli` to print out whatever it is currently doing.<br> This way one can get some context for the error.

## FQBN? Fully Qualified Board Name!

The Fully qualified Board name consists of what Arduino calls the <i>Platform ID</i> and a name for the board. You can find it out either by using `arduino-cli board search <searchterm>`, `arduino-cli board listall` or by appending the official board name in lowercase to the Platform ID.

Examples include:
* `arduino:avr:uno` (Arduino Uno or another board with an ATMega328P running at 16MHz)
* `esp8266:esp8266:generic` (A generic esp8266 Module)
* `esp32:esp32:esp32` (A generic esp32 module)

Please note that the esp boards are not available by default.

## Compile and Upload a Sketch using arduino-cli

I usually create a `compile-and-upload.sh` script to make development and testing easier.

{% figure(caption_after="A simple compile and upload script with a fixed board and an optional argument for the serial port that defaults to <code>/dev/ttyUSB0</code>") %}
```sh
#!/bin/sh
arduino-cli compile --fqbn arduino:avr:uno && \
arduino-cli upload --fqbn arduino:avr:uno -p "${1:-/dev/ttyUSB0}"
```
{% end %}

<b>Note:</b> When given the `-u` flag and the options needed for uploading `arduino-cli compile --fqbn arduino:avr:uno -u -p "${1:-/dev/ttyUSB0}"` behaves like the above example script.

<b>Note on shell syntax:</b> The `"${1:-/dev/ttyUSB0}"` is read by your shell as replace this with the first argument given to the script or with `/dev/ttyUSB0`.

## Adding more Boards

The `arduino-cli` is not just a wrapper for a compiler and whatever you prefer for uploading … it also is a package manager. If you want more boards (or libraries) you can search for and install them.

But if you want to write code for a non-Arduino branded board (i.e. an ESP8266 or ESP32) those packages are not in the Arduino package repositories.

Adding an extra repository can be done by letting `arduino-cli` do the work …

```sh
arduino-cli config add board_manager.additional_urls 'url://to/index.json'
```

… or by manually editing `~/.arduino15/arduino-cli.yaml` and adding the URL pinting to the respositories `*_index.json` file there.

{% figure(caption_after="An excerpt from the configuration file from my laptop with additional URL to the stable esp8266 and esp32 repositories in addition to the development version of the esp32 repository.") %}
```yaml
board_manager:
  additional_urls:
  - https://arduino.esp8266.com/stable/package_esp8266com_index.json
  - https://dl.espressif.com/dl/package_esp32_index.json
  - https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json
```
{% end %}

Don't forget to `arduino-cli update` and `arduino-cli upgrade`!


## Flashing a Bootloader using arduino-cli

`arduino-cli` already knows how to flash a new bootloader with the `burn-bootloader` subcommand, you have to tell it for which board you want the bootloader using `--fqbn` and which programmer you are using with `-P`.

<b>Note:</b> Programmers usually aren't quite cheap, but if you have another Arduino (compatible) with UART (serial) and SPI interface you can use that [Arduino as an <abbr title="In-circuit Serial Programmer">ISP</abbr> ](https://docs.arduino.cc/built-in-examples/arduino-isp/ArduinoISP). (use `-P arduinoasisp`)

It may be necessary to provide the port the programmer is on via the `-p` option. (like with the `upload` command)

{% figure(caption="Example using the official AVRISP mkII programmer for AVR chips for an ATMEL 328P in an Arduino Uno like configuration") %}
```sh
arduino-cli burn-bootloader --fqbn arduino:avr:uno -P avrispmkii
```
{% end %}

To find out which programmer string you need, go to the `ArduinoCore-*` repository of your favourite architecture (See [github.com/arduino](https://github.com/arduino) for the official ones) and open the `programmers.txt` file (i.e. [ArduinoCore-avr/programmers.txt](https://github.com/arduino/ArduinoCore-avr/blob/master/programmers.txt)), the keys used there are the needed ids for the `-P` option.

<b>Note:</b> An interesting option might be to use an [Arduino Microcontroller as your programmer](https://docs.arduino.cc/built-in-examples/arduino-isp/ArduinoISP/), use `-P arduinoisp` in this case. [The neccessary code is also in a public repository.](https://github.com/arduino/arduino-examples/blob/main/examples/11.ArduinoISP/ArduinoISP/ArduinoISP.ino)

### Why do I need a Bootloader?

To be able to seamlessly upload code with just a USB to serial converter like in the example above your microcontroller needs to have the Arduino bootloader installed, a small piece of software that runs when the controller starts (that's the reason you need to hook up the reset line when programming) that can be told to write a new program to the controller instead of starting the already existing one.

The boards you can buy online come with a bootloader preinstalled. But maybe you want to build your own Arduino compatible or released the magic smoke and want to just buy the chip itself without a development-board, that is when you want to flash the bootloader yourself.

### Do I need a Bootloader to use the Arduino Framwork?

No, you can use the programmer of your choice (just use the `-P` option with the upload command) to flash whatever you coded up directly into the controller at the cost of having to use the programmer every time you upload a new program. (Unless that program is a bootloader 😉)

## Using sketch-local Libraries

To use liberies stored anywhere but the default location (i.e. in a `libs` folder next to your `.ino` files) the `--library` and `--libraries` can be used. Both accept a comma seperated list of filepaths.

`--library` points to individual libraries and `--libraries` points to folders of libraries.

{% figure(caption="Example to compile a sketch with a <code>libs</code> folder") %}
```sh
arduino-cli compile --libraries libs/ --fqbn <fqbn>
```
{% end %}

## You may also be interested in

{% linkbutton(href="https://arduino.github.io/arduino-cli/latest/getting-started/")%}The official arduino-cli documantation{% end %}

{% linkbutton(href="@/notebook/serial-not-working.md") %}Serial Port Troublehooting on Linux{% end %}

