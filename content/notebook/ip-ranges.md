+++
    title="IP-Address Ranges"
    description="Overview of reserved IP-Address ranges for IPv4 and IPv6"
    date="2023-03-08"
    [taxonomies]
    notebook=["Cheatsheets"]
    topics=["Networking"]
+++

## In General

While IPv4 and IPv6 have different terminology, they share some concepts. Scope and Delivery schemes.

Both IP-Versions share the following delivery schemes:

{% dl() %}
Unicast 
: one to one, the default

Multicast
: one to many, v6 had a huge Update there

Broadcast
: usually inside the scope of a single network
{% end %}

Scope is a bit version dependant, usually you have:

{% dl() %}
Global
: These addresses are globally routable and assigned by the [IANA](https://www.iana.org/) and the local Internet registries.

Private
: These addresses are reserved for private use and you are free to use the in your own network, just be aware, that you can't use them on the internet.

Loopback
: These addresses always route back to the sending machine, usually `localhost` resolves to them.

Link-Local
: These adresses are automatically assigned so that host on the same network can talk to each other.

Documentation
: These addresses are reserved for documentation purposes.

Other reserved
: These adresses are reserved and have some conditions on when you are supposed to use them.
{% end %}

## IPv6 Adress Ranges

By default IPv6 adresses are unassigned global unicast adresses and therefore reserved for future use. The IANA has a complete list with the [IANA IPv6 Special-Purpose Address Registry](https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xhtml).

{% dl() %}
Global Unicast ([RFC 4291, Section 2.5.4](https://www.rfc-editor.org/rfc/rfc4291.html#section-2.5.4))
: `2000::/3`

Unique Local (Private) ([RFC 4193](https://www.rfc-editor.org/rfc/rfc4193))
: `fd00::/8` (with Random Global ID)
: `fc00::/8` (reserved for future use)

Loopback ([RFC 4291, Section 2.5.3](https://www.rfc-editor.org/rfc/rfc4291.html#section-2.5.3))
: `::1/128`

Link-Local ([RFC 4291, Section 2.5.6](https://www.rfc-editor.org/rfc/rfc4291.html#section-2.5.6)
: `fe80::/10`

Documentation ([RFC 3849](https://www.rfc-editor.org/rfc/rfc3849.html))
: `2001:db8::/32`

Multicast ([RFC 4291, Section 2.7](https://www.rfc-editor.org/rfc/rfc4291.html#section-2.7))
: `ff00::/8`

IETF Protocol Assignments ([RFC 6890](https://www.rfc-editor.org/rfc/rfc6890.html#section-2.1))
: `2001:0000::/23`
{% end %}

<b>Note:</b> Site-Local IPv6 Unicast Addresses ([RFC 4291, Section 2.5.7](https://www.rfc-editor.org/rfc/rfc4291.html#section-2.5.7)) have been deprecated in favour of Unique Local Addresses ([RFC 4193](https://www.rfc-editor.org/rfc/rfc4193)) which are pretty similar, but better defined.

## IPv4 Address Ranges

By default IPv4 adresses are adresses that fall in the global unicast category. The IANA has a complete List with the [IANA IPv4 Special-Purpose Address Registry](https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml#iana-ipv4-special-registry-1).

{% dl() %}
Private ([RFC 1918](https://www.rfc-editor.org/rfc/rfc1918.html))
: `192.168.0.0/16`
: `172.16.0.0/12`
: `10.0.0.0/8`

Loopback ([RFC 1122, Section 3.2.1.3](https://www.rfc-editor.org/rfc/rfc1122.html#page-31))
: `127.0.0.0/8` Note that this is an entire network.
: `127.0.0.1` is usually used.

Link-Local ([RFC 2927](https://www.rfc-editor.org/rfc/rfc3927.html))
: `169.254.0.0/16`

Documentation ([RFC 5737](https://www.rfc-editor.org/rfc/rfc5737.html#section-3))
: `192.0.2.0/24` (TEST-NET-1)
: `198.51.100.0/24` (TEST-NET-2)
: `203.0.113.0/24` (TEST-NET-3)

Shared ([RFC 6598](https://www.rfc-editor.org/rfc/rfc6598.html))
: `100.64.0.0/10` <br><q>used as Shared Address Space to accommodate the needs of Carrier-Grade NAT (CGN) devices.</q>

Benchmarking ([RFC 2544](https://www.rfc-editor.org/rfc/rfc2544.html#appendix-C.2.2.1))
: `198.18.0.0/15`

Multicast
: `240.0.0.0/4` Multicast "Namespace" ([RFC 1112](https://www.rfc-editor.org/rfc/rfc1112.html#section-4))
: `192.52.193.0/24` Automatic Multicast Tunneling ([RFC 7450](https://www.rfc-editor.org/rfc/rfc7450.html))

IETF Protocol Assignments ([RFC 6890](https://www.rfc-editor.org/rfc/rfc6890.html#section-2.1))
: `192.0.0.0/24`
{% end %}

### Broadcast

Broadcast addresses with IPv4 work by setting the host-part of the address to all ones (in the binary representation) they send to all hosts in the local network.

<b>Note:</b> In [RFC 919](https://www.rfc-editor.org/rfc/rfc919.html) `255.255.255.255` is defined as <q>broadcast to all of its immediate neighbors</q>.
