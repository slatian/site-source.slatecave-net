+++
	title = "Slatians Notebook"
	template = "notebook-index.html"
	page_template = "article.html"
	sort_by = "update_date"
	generate_feeds = true

	[extra]
	show_feed_link = true
	short_title = "Notebook"
	show_date = false
+++

A place where I share my knowlege, most pages get updates when I find new and interesting things for them.

