+++
	title = "Acronyms on the Social Internet"
	description = "A little dictionary for random letter collections one might encounter on the Internet."

	date = 2024-12-25

	[taxonomies]
	notebook = ["Cheatsheets"]
+++

<q>TIL: This thing is very useful IMHO</q> … what ???

## Acronyms

{% dl() %}

ALT (Text)
: Alternative Text
: ALT-Text is a kind of image description that describes the <em>meaning</em> of or the <em>intention</em> behind an image (but not necessarily every visual detail) so it helps people who can't see (slow connection, broken link, blind, …) or understand image for some reason.
: It can also be used as an art-form where the ALT-Text serves its original function **and** is itself worth reading because it is written in a creative way.

alt (Account)
: Alternative/Secondary Account
: Some people have multiple Accounts they publish on or openly keep as backups, these may be referred to as alt accounts or <q>alts</q>.

CW
: [Content Warning](https://en.wikipedia.org/wiki/Trauma_trigger#Trigger_warnings)

DM
: Direct Message
: Message sent with restricted visibility to only a few intended recipients.

IMO
: In My Opinion

IMHO
: In My Honest Opinion

OH
: Over Heard
: Usually used as a prefix for posts quoting something funny someone overheard.
: The variation <q>Self OH</q> means someone is quoting themselves.

RFC
: Request For Comments
: Either refers to the [IETF/IRTF/IAB RFC specifications](https://www.rfc-editor.org/), a project specific RFC system or a standalone document someone wants feedback on.

TIL
: Today I Learned
: Used as a prefix for posts where someone who just learned about something is sharing that information. Usually because that information is considered useful.
{% end %}

This list isn't complete, it'll be expanded as people ask me or I learn about things myself.

Also see the list of Acronyms for content warnings below.

## Tone Indicators

Tone indicators like `/s` are sometimes appended to the end of a message to signal the intention behind a message (i.e. sarcasm), because on the internet things aren't always obvious (evidence: you are reading this article). They always start with a forward slash `/`.

The most important/common ones are:

{% dl() %}
`/s`
: sarcasm

`/j`
: joking

`/hj`
: half joking
{% end %}

{% linkbutton(href="https://en.wikipedia.org/wiki/Tone_indicator" icon="action-next") %}Wikipedia has a more complete list and background information.{% end %}

## Acronyms for Content Warnings

Content warnings are an important mechanism to let people opt out of content they might find uncomfortable to very stressing. To keep the needed effort low a bunch of acronyms to tag common

{% dl() %}
`+`
: Positive Content
: Used as a suffix the poster thinks the content behind the warning is on the more positive side of things.

`-`
: Negative Content
: Used as a suffix the poster thinks the content behind the warning is on the more negative side of things.

ph
: Physical Health
: Usually content mentioning injuries or other physical health issues

mh
: Mental Health
: Content mentioning mental health issues

pol, depol, uspol, …
: Politics
: Usually used as part of <i>Content Warning</i> messages to flag messages mentioning politics, it can be prefixed by a [two or three letter country reference](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes) to quickly communicate which country is talked about.

{% end %}

