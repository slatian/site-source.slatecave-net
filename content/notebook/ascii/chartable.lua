local char_index = tonumber(io.read())

local output_text=""
local has_name_column = false
local has_c_escape = false

while true do
	local i = io.read()
	if not i then break end
	name_column=""
	c_escape = ""
	if i:match("\t") then
		i,name = i:match("(.-)\t(.+)")
		name = name:gsub(" `(.-)`", function (s) c_escape = s return "" end)
		name_column = "<td class=\"name\">%s</td>"
		has_name_column=true
	end
	output_text = output_text..("<tr><td>%s</td><td>%02x</td><td>%03o</td><td>%d</td>"..name_column):format(i, char_index, char_index, char_index, name)
	if c_escape ~= "" then
		has_c_escape = true
		output_text = output_text.."<td><code>"..c_escape.."</code></td>"
	end
	output_text = output_text.."</tr>\n"
	char_index = char_index + 1
end

print('<table class="character-table">')
print("<tr><th>Char</th><th>Hex</th><th>Oct</th><th>Dec</th>"..(has_name_column and "<th>Name</th>" or "")..(has_c_escape and "<th>C Esc</th>" or "").."</tr>")
print(output_text.."</table>")
