+++
	title = "ASCII Table"
	description = "A simple, plain ASCII-Table"
	date = "2022-10-16"
	updated = "2024-08-05"
	[extra]
	banner="interoperability"
	[taxonomies]
	notebook=["Cheatsheets"]
+++

This ASCII-Table is mostly derived from the [Unicode 15.0 Documentation](https://www.unicode.org/Public/UCD/latest/).

## Mostly Printable Characters

{% linkbutton(href="#control-characters",icon="action-next") %}Skip to the control characters table{% end %}

Codes 0x20 to 0x7E are referred to as the printable characters, 0x7F is a control character. [Wikipedia has more background information.](https://en.wikipedia.org/wiki/ASCII#Printable_characters)

<div class="character-tables">
<table class="character-table">
<caption>Numbers and Specials</caption>
<tr><th>Char</th><th>Hex</th><th>Dec</th></tr>
<tr><td>SPACE</td><td>20</td><td>32</td></tr>
<tr><td>!</td><td>21</td><td>33</td></tr>
<tr><td>"</td><td>22</td><td>34</td></tr>
<tr><td>#</td><td>23</td><td>35</td></tr>
<tr><td>$</td><td>24</td><td>36</td></tr>
<tr><td>%</td><td>25</td><td>37</td></tr>
<tr><td>&</td><td>26</td><td>38</td></tr>
<tr><td>'</td><td>27</td><td>39</td></tr>
<tr><td>(</td><td>28</td><td>40</td></tr>
<tr><td>)</td><td>29</td><td>41</td></tr>
<tr><td>*</td><td>2a</td><td>42</td></tr>
<tr><td>+</td><td>2b</td><td>43</td></tr>
<tr><td>,</td><td>2c</td><td>44</td></tr>
<tr><td>-</td><td>2d</td><td>45</td></tr>
<tr><td>.</td><td>2e</td><td>46</td></tr>
<tr><td>/</td><td>2f</td><td>47</td></tr>
<tr><td>0</td><td>30</td><td>48</td></tr>
<tr><td>1</td><td>31</td><td>49</td></tr>
<tr><td>2</td><td>32</td><td>50</td></tr>
<tr><td>3</td><td>33</td><td>51</td></tr>
<tr><td>4</td><td>34</td><td>52</td></tr>
<tr><td>5</td><td>35</td><td>53</td></tr>
<tr><td>6</td><td>36</td><td>54</td></tr>
<tr><td>7</td><td>37</td><td>55</td></tr>
<tr><td>8</td><td>38</td><td>56</td></tr>
<tr><td>9</td><td>39</td><td>57</td></tr>
<tr><td>:</td><td>3a</td><td>58</td></tr>
<tr><td>;</td><td>3b</td><td>59</td></tr>
<tr><td>&lt;</td><td>3c</td><td>60</td></tr>
<tr><td>=</td><td>3d</td><td>61</td></tr>
<tr><td>&gt;</td><td>3e</td><td>62</td></tr>
<tr><td>?</td><td>3f</td><td>63</td></tr>
</table>
<table class="character-table">
<caption>Uppercase</caption>
<tr><th>Char</th><th>Hex</th><th>Dec</th></tr>
<tr><td>@</td><td>40</td><td>64</td></tr>
<tr><td>A</td><td>41</td><td>65</td></tr>
<tr><td>B</td><td>42</td><td>66</td></tr>
<tr><td>C</td><td>43</td><td>67</td></tr>
<tr><td>D</td><td>44</td><td>68</td></tr>
<tr><td>E</td><td>45</td><td>69</td></tr>
<tr><td>F</td><td>46</td><td>70</td></tr>
<tr><td>G</td><td>47</td><td>71</td></tr>
<tr><td>H</td><td>48</td><td>72</td></tr>
<tr><td>I</td><td>49</td><td>73</td></tr>
<tr><td>J</td><td>4a</td><td>74</td></tr>
<tr><td>K</td><td>4b</td><td>75</td></tr>
<tr><td>L</td><td>4c</td><td>76</td></tr>
<tr><td>M</td><td>4d</td><td>77</td></tr>
<tr><td>N</td><td>4e</td><td>78</td></tr>
<tr><td>O</td><td>4f</td><td>79</td></tr>
<tr><td>P</td><td>50</td><td>80</td></tr>
<tr><td>Q</td><td>51</td><td>81</td></tr>
<tr><td>R</td><td>52</td><td>82</td></tr>
<tr><td>S</td><td>53</td><td>83</td></tr>
<tr><td>T</td><td>54</td><td>84</td></tr>
<tr><td>U</td><td>55</td><td>85</td></tr>
<tr><td>V</td><td>56</td><td>86</td></tr>
<tr><td>W</td><td>57</td><td>87</td></tr>
<tr><td>X</td><td>58</td><td>88</td></tr>
<tr><td>Y</td><td>59</td><td>89</td></tr>
<tr><td>Z</td><td>5a</td><td>90</td></tr>
<tr><td>[</td><td>5b</td><td>91</td></tr>
<tr><td>\</td><td>5c</td><td>92</td></tr>
<tr><td>]</td><td>5d</td><td>93</td></tr>
<tr><td>^</td><td>5e</td><td>94</td></tr>
<tr><td>_</td><td>5f</td><td>95</td></tr>
</table>
<table class="character-table">
<caption>Lowercase</caption>
<tr><th>Char</th><th>Hex</th><th>Dec</th></tr>
<tr><td>&#96;</td><td>60</td><td>96</td></tr>
<tr><td>a</td><td>61</td><td>97</td></tr>
<tr><td>b</td><td>62</td><td>98</td></tr>
<tr><td>c</td><td>63</td><td>99</td></tr>
<tr><td>d</td><td>64</td><td>100</td></tr>
<tr><td>e</td><td>65</td><td>101</td></tr>
<tr><td>f</td><td>66</td><td>102</td></tr>
<tr><td>g</td><td>67</td><td>103</td></tr>
<tr><td>h</td><td>68</td><td>104</td></tr>
<tr><td>i</td><td>69</td><td>105</td></tr>
<tr><td>j</td><td>6a</td><td>106</td></tr>
<tr><td>k</td><td>6b</td><td>107</td></tr>
<tr><td>l</td><td>6c</td><td>108</td></tr>
<tr><td>m</td><td>6d</td><td>109</td></tr>
<tr><td>n</td><td>6e</td><td>110</td></tr>
<tr><td>o</td><td>6f</td><td>111</td></tr>
<tr><td>p</td><td>70</td><td>112</td></tr>
<tr><td>q</td><td>71</td><td>113</td></tr>
<tr><td>r</td><td>72</td><td>114</td></tr>
<tr><td>s</td><td>73</td><td>115</td></tr>
<tr><td>t</td><td>74</td><td>116</td></tr>
<tr><td>u</td><td>75</td><td>117</td></tr>
<tr><td>v</td><td>76</td><td>118</td></tr>
<tr><td>w</td><td>77</td><td>119</td></tr>
<tr><td>x</td><td>78</td><td>120</td></tr>
<tr><td>y</td><td>79</td><td>121</td></tr>
<tr><td>z</td><td>7a</td><td>122</td></tr>
<tr><td>{</td><td>7b</td><td>123</td></tr>
<tr><td>|</td><td>7c</td><td>124</td></tr>
<tr><td>}</td><td>7d</td><td>125</td></tr>
<tr><td>~</td><td>7e</td><td>126</td></tr>
<tr><td>DEL</td><td>7f</td><td>127</td></tr>
</table>
</div>

<b>Note on the `DEL` code:</b>The DEL being 0x7F, meaning it has all 7 bits set, probably originates from how one "deleted" something from paper tape, by just punching all of the holes one could "erase" the original character. Its meaning is ambigious, but it usually refers to a backspace.

!note: <b>Fun Fact:</b> The lowercase characters in ASCII were an "aftertought" and only added later in 1965, two years after the initial release.

## Control characters

<table>
<tr><th>Char</th><th>Hex</th><th>Oct</th><th>Dec</th><th>Name</th><th>C Esc</th></tr>
<tr><td>NUL</td><td>00</td><td>000</td><td>0</td><td class="name">Null</td></tr>
<tr><td>SOH</td><td>01</td><td>001</td><td>1</td><td class="name">Start Of Heading</td></tr>
<tr><td>STX</td><td>02</td><td>002</td><td>2</td><td class="name">Start Of Text</td></tr>
<tr><td>ETX</td><td>03</td><td>003</td><td>3</td><td class="name">End Of Text</td></tr>
<tr><td>EOT</td><td>04</td><td>004</td><td>4</td><td class="name">End Of Transmission</td></tr>
<tr><td>ENQ</td><td>05</td><td>005</td><td>5</td><td class="name">Enquiry</td></tr>
<tr><td>ACK</td><td>06</td><td>006</td><td>6</td><td class="name">Acknowledge</td></tr>
<tr><td>BEL</td><td>07</td><td>007</td><td>7</td><td class="name">Bell / Alert</td><td><code>\a</code></td></tr>
<tr><td>BS</td><td>08</td><td>010</td><td>8</td><td class="name">BAckspace</td></tr>
<tr><td>HT</td><td>09</td><td>011</td><td>9</td><td class="name">HOrizontal Tab</td><td><code>\t</code></td></tr>
<tr><td>LF</td><td>0a</td><td>012</td><td>10</td><td class="name">Line Feed</td><td><code>\n</code></td></tr>
<tr><td>VT</td><td>0b</td><td>013</td><td>11</td><td class="name">Vertical Tab</td><td><code>\v</code></td></tr>
<tr><td>FF</td><td>0c</td><td>014</td><td>12</td><td class="name">Form Feed</td><td><code>\f</code></td></tr>
<tr><td>CR</td><td>0d</td><td>015</td><td>13</td><td class="name">Carriage Return</td><td><code>\r</code></td></tr>
<tr><td>SO</td><td>0e</td><td>016</td><td>14</td><td class="name">Shift Out</td></tr>
<tr><td>SI</td><td>0f</td><td>017</td><td>15</td><td class="name">Shift In</td></tr>
<tr><td>DLE</td><td>10</td><td>020</td><td>16</td><td class="name">Data Link Escape</td></tr>
<tr><td>DC1</td><td>11</td><td>021</td><td>17</td><td class="name">Device Control 1</td></tr>
<tr><td>DC2</td><td>12</td><td>022</td><td>18</td><td class="name">Device Control 2</td></tr>
<tr><td>DC3</td><td>13</td><td>023</td><td>19</td><td class="name">Device Control 3</td></tr>
<tr><td>DC4</td><td>14</td><td>024</td><td>20</td><td class="name">Device Control 4</td></tr>
<tr><td>NAK</td><td>15</td><td>025</td><td>21</td><td class="name">Negative Acknowledge</td></tr>
<tr><td>SYN</td><td>16</td><td>026</td><td>22</td><td class="name">Synchronous Idle</td></tr>
<tr><td>ETB</td><td>17</td><td>027</td><td>23</td><td class="name">End Of Transmission Block</td></tr>
<tr><td>CAN</td><td>18</td><td>030</td><td>24</td><td class="name">Cancel</td></tr>
<tr><td>EM</td><td>19</td><td>031</td><td>25</td><td class="name">End Of Medium</td></tr>
<tr><td>SUB</td><td>1a</td><td>032</td><td>26</td><td class="name">Substitute</td></tr>
<tr><td>ESC</td><td>1b</td><td>033</td><td>27</td><td class="name">Escape</td><td><code>\e</code></td></tr>
<tr><td>FS</td><td>1c</td><td>034</td><td>28</td><td class="name">File Seperator</td></tr>
<tr><td>GS</td><td>1d</td><td>035</td><td>29</td><td class="name">Group Seperator</td></tr>
<tr><td>RS</td><td>1e</td><td>036</td><td>30</td><td class="name">Record Seperator</td></tr>
<tr><td>US</td><td>1f</td><td>037</td><td>31</td><td class="name">Unit Seperator</td></tr>
</table>

<b>Note:</b> The Names have been titlecased to make them easier to read.

## Notation

### Escaping in Scripting and Programming

Sometimes in languages one can't use all the characters available in the source file, especially when using control characters. Encoding these as other characters is called <i>escaping</i>. This usually happens with text between double quotes `"`.

There are differences between languages and implementations but the general rules are:
* Special characters preceeded by a backslash `\` are taken literally. (i.e. `\"` or `\\`) (The exact behaviour varies *a lot* by language.)
* Latin characters preceeded by a backslash to generate control characters (i.e. `\n`) (The <i>C Esc</i> column)
* Hexadecimal value of the character prefixed by `\x` (i.e. `0x1b`) (should work almost everywhere)
* Octal encoding prefixed by just a backslash (i.e. `\033`) ([Use this when using `printf` on the command line](/notebook/ansi-escape-sequences/#setting-text-color-and-effects))

<b>Note:</b> `C Esc` here is short C-Escape as the C language apparantly started this way of excaping characters in strings. It is now used by almost all modern languages and in other contexts. i.e. `printf`, `sed`, etc. exact support may vary.

### Escaping in XML and HTML

XML and html have their own way of escaping non-printable characters, this involves a sequence sandwiched between an ampersand `&` and a semicolon `;`.

In general Characters can be escaped using `&#<dec>;` where `<dec>` is replaced by the decimal value associted with the character. (i.e, `&#38;` to encode an ampersand `&`)

There are also named escapes to make remembering them easier:

<table>
<tr><th>Character</th><th>XML-escape</th></tr>
<tr><td><code>&amp;</code></td><td><code>&amp;amp;</code></td></tr>
<tr><td><code>&lt;</code></td><td><code>&amp;lt;</code></td></tr>
<tr><td><code>&gt;</code></td><td><code>&amp;gt;</code></td></tr>
<tr><td><code>&quot;</code></td><td><code>&amp;quot;</code></td></tr>
</table>

### Control and Shift

With the ASCII table control and shift keys an be implemented using simple addition and substraction.

The lowercase character can be obtained by adding 32 (0x20) to the code of the corresponding uppercase character and the control key goes 64 (0x40) in the opposite direction.

With that <kbd>ctrl+c</kbd> maps to code 3 "End of Text". And <kbd>ctrl+d</kbd> maps to "End of Transmission". You may know those shortcuts from the terminal, they hopefully make a bit more sense now.

Control characters are sometimes written down/printed as the character one gets when adding 64 to the control characters value prefixed by an `^`.

This maps escape to `^]` and the nullbyte to `^@`. (You have probably seen those when opening a binary file in a text editor.)

## Unicode

Creating a unicode table andkeeping it updated is out of scope here, besides that: [Wikipedia has a List of Unicode Characters](https://en.wikipedia.org/wiki/List_of_Unicode_characters) and there are the [offical Unicode Character Code Charts](https://www.unicode.org/charts/). (Plus a whole lot of other unicode tables out there.)

!note: <b>Notation Hint:</b> Unicode codepoints (that is the number assigned in the unicode standard, not the binary character representation) are written `U+<hex>` where `<hex>` is the codepint number in hexadecimal. Example: `U+1f603` for 😃.

There is also a little commandline tool called [`uni`](https://github.com/arp242/uni), that is pretty good at providing a searchable unicode table.
