+++
    title = "Webdirectories"
    description = "A collection of link collections I consider worth sharing"
    date = 2022-11-03
    updated = 2025-02-01
    
	[taxonomies]
	notebook = ["Bookmarks"]
	topics = ["Web", "Search Engines"]
+++

## Webrings

Listed roughly in order of preference, it is *NOT* a ranking. Prefernce in this case means: In therms of how many sites I personally conider interesting. Webrings I found more recently or just don't explore as often will naturally rank lower.

In case you are new to webrings: Webrings are sometimes members of other webrings and a few sites are members of a lot of webrings so the below are just some possible entry points.

### Geekring

{{ linkbutton(href="https://geekring.net") }}

> For all those funny, quirky, interesting, personal, strange, offbeat websites that's not page 1 on the search engines.

### Hotline Webring

<b>Has no restrictions for joining and may contain objectionable content.</b>

{{ linkbutton(href="https://hotlinewebring.club/") }}

Geekring with more retro themed websites.

### The retronaut webring

<b>Has no restrictions for joining and may contain objectionable content.</b>

{{ linkbutton(href="https://webring.dinhe.net/") }}

A webring that contains all kinds of sites.

### The Yesterweb Ring

{{ linkbutton(href="https://yesterweb.org/webring/") }}

> This webring is for anyone who is tired of how boring and same-y the internet is today. It's for anyone who is sick of seeing websites used purely to drive monetization, informative blogs that ask you to subscribe to see content.

### XXIIVV Webring

{{ linkbutton(href="https://webring.xxiivv.com/") }}

>  This webring is an attempt to inspire artists & developers to build their websites and share traffic amongst each other. The ring welcomes hand-crafted wikis and portfolios.

### The 512KB Club

{{ linkbutton(href="https://512kb.club/") }}

> The 512KB Club is a collection of performance-focused web pages from across the Internet.

More of a directory but sometimes considered a webring (it's missin the ring-part though)

### envs - webring

{{ linkbutton(href="https://envs.net/ring/") }}

> this webring can be joined by any user on envs.net.

## Actual web directories

The real phonebooks of the internet!

### ooh.directory

{{ linkbutton(href="https://ooh.directory/") }}

> A collection of 2,295 blogs about every topic

The counter in the description is probably outdated by the time you are reding this 😃.


## Sites with lots of interesting links

### Seirdy’s Home

{{ linkbutton(href="https://seirdy.one/") }}

Not only a source of great links but also great articles containing them.

### TinyGem

{{ linkbutton(href="https://tinygem.org/") }}

> Have you ever bookmarked a page and worried that you will not find the same stuff next time you are there? Keep the pages on the web saved forever.

A searchable link aggregator for links people find interesting.
