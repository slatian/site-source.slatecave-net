+++
	title="Podcasts"
	description="A list of podcasts I listen to and can recommend"
	date="2022-11-06"
	[taxonomies]
	notebook=["Bookmarks"]
+++

Note that these are mostly about IT and (I think) well known podcasts.

## English Language Podcasts

### FOSS and Crafts
{{ linkbutton(href="https://fossandcrafts.org/") }}
> FOSS and Crafts is an interdisciplinary exploration of collaborative creation. […]  We explore such topics as computer programming, craft production, user freedom and agency, especially informed by the principles of the F(L)OSS (Free, Libre, and Open Source Software) and free culture movements.

### postmarketOS Podcast
{{ linkbutton(href="https://cast.postmarketos.org/") }}
> You just found the corner of the Internet, where postmarketOS team members discuss whatever crazy development details we encounter while cooking up our favorite mobile Linux distribution. Besides news and anecdotes, once in a while we throw in an interview with an exciting guest who brings us their unique view on the wider mobile Linux scene.

## Deutschsprachige Podcasts

<!--LANGUAGE:de-->

### Chaosradio
{{ linkbutton(href="https://chaosradio.de/") }}
> Chaosradio ist der Podcast des Chaos Computer Club Berlin (CCCB). In dem (bis zu) zweistündigen Format informieren wir seit 1995 über technische und gesellschaftliche Themen. [Weiterlesen …](https://chaosradio.de/about)

### C-RadaR
{{ linkbutton(href="https://www.c-radar.de/") }}
> Monatliche Radiosendung des Chaos Computer Clubs auf Radio Darmstadt. Jeden 2ten Donnerstag im Monat, 21-23 Uhr. 103,4 MHz UKW / DAB+ / Stream. Tune In!

### Pentaradio
{{ linkbutton(href="https://www.c3d2.de/radio.html") }}
> Magazin für Netzkultur und Politik des Chaos Computer Clubs Dresden C3D2.
> jeden vierten Dienstag im Monat 21:30 – 23:00 Uhr
> <cite>(Beschreibung: [coloRadio](https://coloradio.org/?page_id=5199))</cite>


