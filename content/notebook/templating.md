+++
	title="Templating Zoo"
	description="Stuck with some undocumented templating?"
	date=2024-07-02
	[taxonomies]
	notebook=["Cheatsheets"]
+++

## What is Templating?

Templating in the sense of this articlee is when there is a text with placeholders that get filled in according to some set of rules (the <i>templating language</i>). Using templating is often easier than hardcoding some string assembly logic, it also has the benefit that the template could be read in from a configuration file.

In case you've ended up here because you have some sample templating but no documentation: Keep reading, you'll probably find what you're searching for. (No gurantees though, there are many templating mechanisms out there)

Since most Templating languages borrow from each other it makes sense to group them into Families.

## The printf likes

The [printf()](https://en.wikipedia.org/wiki/Printf) function makes it easy to generate semi-templated text in C and other languages without having to do manual string manipulaion. These templates are sometimes even applicable in reverse like with [scanf](https://en.wikipedia.org/wiki/Scanf) or [strptime](https://man.voidlinux.org/man3/strptime.3).

{% dl() %}
How to recognize
: The placeholders usually start with a percent sign (`%`) and end in a single letter that depends on the datatype. Extra information on how exactly to format is between the two.
: Special characters may be escapable using [backslash (`/`) escape sequences](/notebook/ascii/#escaping-in-scripting-and-programming).

Examples
: `Template that contains a string: %s`
: `Formatted number: %.2f`
: `Literal percent sign: %%`

Languages
: printf likes are implemented in many places
: The fully featured printf is likely to be found around C programs or in scripting languages like [`string.format()` in lua](https://www.lua.org/manual/5.4/manual.html#pdf-string.format) or the [printf shell command](https://man.voidlinux.org/printf).

Documentation
: [printf](https://en.wikipedia.org/wiki/Printf#Format_specifier) for generic number and string formatting
: [strftime](https://man.voidlinux.org/strftime.3) for formatting timestamps (quite popular across different libraries)
: custom formats like for the [find command](https://man.voidlinux.org/find#printf) also exist, those are usually documented in a manual somewhere
{% end %}

!warning: <b>Compatibility Warning:</b> printf implementations are not consistent and not all implementations support all features. For example encoding characters as octal (i.e. `/033`) is more widely supported than its hexadecimal cousin (i.e. `\x1B`).

### Python printf-style

[Python has "printf-style String Formatting"](https://docs.python.org/3.12/library/stdtypes.html#old-string-formatting) using the `%`-operator. The printf syntax has been extended to allow indexing dictionaries and is usually encoutered as `%(key)s` where `key` is the key used to index the dictionary and `s` is an example of the most common printf type, but all other types work too, if the data allows.

While **this approach is dicouraged** (in favor of pythons new format function) it will be used for quite some time in configuration files. 

## Go-Templates

Go-templates are named after ther implementation in the go programming language, the are part of the de-facto standard library. They are best known from the [Hugo static site generator](https://gohugo.io/), though the hugo extension usually aren't reimplemented elsewhere. The template packages by themselves are pretty barebones.

{% dl() %}
How to recognize
: **All** template syntax happens eclosed between `{{` and `}}`
: Placeholders usually start with a dot (`.`)

Examples
: `{{.Count}} items are made of {{.Material}}`
: `{{23 -}} < {{- 45}}` renders to `23<45`
: `Hello, {{.}}!`
: `{{ if and (eq .View "search") (ne .Data.Site "") }}…{{ end }}`

Langugage
: Go

Documentation
: [text/template](https://pkg.go.dev/text/template)
: [html/template](https://pkg.go.dev/html/template)
{% end %}

## Jinja2 and friends

[Jinja2](https://jinja.palletsprojects.com/en/) is a flexible and popular templating engine, that is used almost everywhere, where moere advanced templating is used. It also heavily inspired the [tera crate for rust](https://keats.github.io/tera/), which has almost identical syntax.

{% dl() %}
How to recognize
: Variables are enclosed in `{{` and `}}`, template logic uses `{%` and `%}`
: Templates based on other templates have `{% extends "…" %}` as their first line.
: Variables don't start with a dot (`.`) (that''s a go template thing)

Examples
: `This morning I need {{ size | default(value="a huge cup of" }} coffe.`
: `{% if thing.is_working %}🥳{% else %}🔧{% endif %}`

Languages
: Python
: Rust

Documentation
: [Jinja2](https://jinja.palletsprojects.com/en/)
: [tera](https://keats.github.io/tera/docs/)

Where to find
: In web-related contexts, generating HTML or XML
: Configuration templates, for example in [Ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/template_module.html)
: In other contexts too …
{% end %}

## XSLT

[XSLT (Extensible Stylesheet Language Transformations)](https://en.wikipedia.org/wiki/XSLT) is usually used to convert XML to different XML, but it can also be (ab)used to convert XML to text.

{% dl() %}
How to recognize
: The template is XML and mentions the `xsl:` namespace (`http://www.w3.org/1999/XSL/Transform`) a lot.

Example
: See the [Wikipedia Article on XSLT](https://en.wikipedia.org/wiki/XSLT#Examples)

Languages
: Java, but not exclusiely

Where to find
: In enterprisey apllications from the 90s
: [In your web-browser](/blog/atom-xslt/)
{% end %}

