+++
	title="ATmega328P and the Arduino UNO R3"
	description="Pinout of the ATmega328P and how it maps to the Arduino UNO R3."
	date=2024-03-31
	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Electronics and Microcontroller"]
+++

Pinout of the ATmega328P and how it maps to the Arduino UNO R3.

<b>Note:</b> According to the Datasheet most of this Information also applies to the ATmega328, ATmega168(P)A, ATmega88(P)A and ATmega48(P)A.

## Pinout Table

The following Table applies to the 28 Pin DIP Packaged ATmega328. The Arduino Pin mapping is based on the [Arduino UNO R3](https://docs.arduino.cc/hardware/uno-rev3/).

{{ pinout_table(path="notebook/ATmega328P/pins.toml") }}

{% dl() %}

Pin
: Physical Pin on the ATmega328P DIP Package.

Name
: The Pin name in the ATmega328 Datasheet.

Arduino Pin
: The Pin number/name assigned by Arduino.

PWM
: Which oscillator the Pin is connected to. If it is connected to one it supports PWM.

PCINT
: The Pin Change Interrupt number assigned to the pin.

Bus
: Bus protocol Hardware the pin is connected to.

Extra
: Any extra features the Pin might have or a short explanation.

{% end %}

<b>Note on SPI Nomenclature:</b> Instead of the old "Master" and "Slave" terminology on SPI you can use <i>Main</i> and <i>Secondary</i> while keeping all the old Acronyms. 😉

## Barebones Arduino UNO R3 compatible

Maybe you've built something awesome and now you want to get it off that bulky Arduino board and for some reason the individual parts are cheaper than a tiny Arduino compatible board (they used to be, guess who has a stash of ATmega328P chips).

### Needed Parts

To build a barebones Arduino UNO R3 compatible you need:

* One ATmega328P
* Something to flash the Arduino bootloader
* Power circuit:
	* A stable 5V Power supply (I.e. from USB)
	* Two optional 100nF Capacitors
	* One optional 10μH Inductor
* Clock Circuit:
	* One 16MHz Crystal
	* Two 22pf Capacitors
* Reset Circuit
	* One button (open by default)
	* One 10KΩ (roughly) Resistor
	* One optional 100nF Capacitor

### Building the circuit

{{ inline_svg(
	path="./barebones_ATmega328P_circuit_current_color.svg",
	labels_are_incomplete_or_broken=true,
	caption_after="Schematic of the ATmega328P hooked up to power, an external crystal, and a reset circuit as described below. The Serial interface for programming is illustrated as an FTDI compatible layout."
)}}

For the power Connect <i>VCC</i> and <i>AVCC</i> to 5V and the two <i>GND</i> to Ground. Leave <i>AREF</i> floating unless you need it.

If you want that extra bit of reliability connect a 100nF Capacitor between <i>VCC</i> and <i>GND</i> and between <i>AVCC</i> and <i>GND</i>. The official UNO has a 10μH inductor between <i>AVCC</i> and the 5V to stabilise it even more.

<b>Note on AREF:</b> The official Arduino boards connect AREF through 100nF Capacitor to ground. I assume this is to initially pull it to Ground while allowing you to override that setting by simply applying a voltage.

For the Clock connect the 16MHz crystal between <i>XTAL1</i> and <i>XTAL2</i>, and each of <i>XTAL1</i> and <i>XTAL2</i> through a 22pf Capacitor to Ground. Try to no make the circuit paths too long here.

For the Reset Circuit: Connect the <i>Reset</i> pin through a 10KΩ Resistor to 5V, put the button between <i>Reset</i> and Ground.

When using a Serial interface (Make sure it uses 5V logic levels, otherwise you'll destroy something), put the 100nF Capacitor between the <i>Ready to Send</i> (<i>RTS</i> or <i>DTR</i>) and the Reset pin, this way the Chip will automatically reset when the Serial line becomes active, which is the exact behaviour you want when programming using the Arduino bootloader.

<b>Note on perfboard Layout:</b> On a perfboard I usually try to go as compact as possible and have a male header right up to the microcontrollers pins that gives access to GND, Reset, RX and TX (with VCC being part of a male GND, VCC, GND header). The DTR capacitor is on a little adaptor board along with an indicator LED that then plugs into my FTDI breakout. The reset button is near the Reset pin and the pull-up resistor goes below the IC-Socket.

### Flashing the Bootloader

To flash the Arduino bootloader onto the ATmega328P you have to hook up the SPI interface to a programmer. See the [official Arduino tutorial](https://docs.arduino.cc/built-in-examples/arduino-isp/ArduinoToBreadboard/) for this step or my [tutorial for doing the same using `arduino-cli`](/notebook/arduino-cli/#flashing-a-bootloader-using-arduino-cli).


## PWM and Timer/Counters

The ATmega328P has 3 so called <i>Timer/Counter</i> units, called <i>Timer/Counter0</i>, <i>Timer/Counter1</i> and <i>Timer/Counter2</i> each can drive 2 of 6 PWM channels.

I'll abbreviate them as <i>t/c</i> in the following.

Oversimplified they are counters hooked up to a configurable Clock source. Every time the clock ticks the counter increments by one. Each counter has two comperators to trigger actions when the counter has reached a configured point.

The actions that can be triggered may be:
* Interrupts to the CPU
* A PWM Signal that can be output on a pin.
* Reset the counter

<i>t/c0</i> and <i>t/c2</i> have an 8-bit counter while <i>t/c1</i> has a 16-bit counter (it can be put in 8-bit and 12-bit modes too).

The clock source itself is too complicated to explain as there are many options, best explained by the official Datasheet. Usually the system clock is used. Each <i>t/c</i> also has a prescaler that can be used to make it run slower than the system by a certain factor.

There are a few modes the counters can be in:

{% dl(compact=false) %}

Normal
: Simply counts up and does something when the timer overflows and wraps back to 0

Clear Timer on Compare Match (CTC)
: Counts up, but wraps to 0 and does something when the value in ine of the control registers matches

Fast PWM
: Counts up, on a first match the PWM signal is turned off, on a second or overflow it is turned back on.

Phase Correct PWM Mode, Timing Diagram
: Counts up and instead of wrapping down again, the PWM signal is toggled at a comparison point.

{% end %}

For The Phase Correct PWM Mode, the center of the wave always stays in the same place, which is good for driving motors. It also effectively halfes the frequency of the <i>t/c</i> in addition to whatever the prescaler does.

### PWM and Arduino

The Arduino has 6 PWM capable pins, each connected to one comparator channel of the three timers.

By default all of the <i>t/cs</i> are in 8-bit resolution Phase Correct PWM with clock division by 64 in the prescaler, wrapping at the 8-bit overflow (after 256 cycles), which reults in a [PWM frequency of ~490Hz](https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/).

The exception here is <i>t/c0</i> driving the counter behind the [`millis()` function](https://www.arduino.cc/reference/en/language/functions/time/millis/) ([code](https://github.com/arduino/ArduinoCore-avr/blob/master/cores/arduino/wiring.c#L65)) is in Fast PWM Mode. This is why Arduino Pins 5 and 6 have double the frequency at ~980Hz.

{% figure(caption="Calculation of how to get from the System frequency to the PWM frequency.") %}
```
16MHz / 64 / 2 / 256 = ~488Hz
```
{% end %}

#### Tone generation

The [`tone()` function](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/) uses <i>t/c2</i> which is why it messes up the PWM on Arduino Pins 3 and 11.

[The underlying code](https://github.com/arduino/ArduinoCore-avr/blob/63092126a406402022f943ac048fa195ed7e944b/cores/arduino/Tone.cpp#L243) adjusts the clock divider and <i>Compare Registers</i> to select an appropriate one for the desired frequency, and puts the timer into <i>Clear Timer on Compare Match</i> Mode to generate generate an unmodulated square wave.

!note: <b>Assumption:</b> Based on how the code looks it is now capable of using any PWM pin for the tone generating and mess up the the corresponding other PWM pin. I've not bothered to prove or disprove this yet, do your own research.

#### Avoiding PWM flickering

When connecting an RGB LED to a combination of <i>t/c0</i> and one of the other timers to generate pretty colors you might notice an annoying flicker which comes from you seeing some interference pattern that stems from the different modes.

Since you really shouldn't change the configuration of <i>t/c0</i> you can put one of the other timers in Fast PWM mode (the LEDs don't care).

{% figure(caption="Example that put's <i>t/c1</i> into Fast PWM mode given the Arduino default configuration.")%}
```cpp
// Set timer/counter 1 from 8-bit phase correct to 8-bit fast
bitWrite(TCCR1B, WGM12, 1);
```
{% end %}

{% linkbutton(href="/blog/fixing-pwm-flicker/", icon="action-next")%}See the blogpost on fixing PWM flicker for an explanation why it flickers.{% end %}


## Useful Resources

{% linklist(icon="action-next") %}
* [ATmega328P Datasheet and Application Notes](https://www.microchip.com/en-us/product/atmega328p#document-table)
* [Direct Link to the ATmega328P Datasheet](https://www.microchip.com/DS40002061B)
* [Arduino UNO R3 Documentation](https://docs.arduino.cc/hardware/uno-rev3/)
* [ArduinoCore-avr Source Code](https://github.com/arduino/ArduinoCore-avr/tree/master)
{% end %}

