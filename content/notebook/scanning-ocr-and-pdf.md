+++
    title="Scanning, OCR and PDF"
    description="A cheatsheet for scanning and and OCRing documents on Linux with sane and tesseract"
    date="2023-04-24"
    updated=2024-05-19
    [taxonomies]
    notebook=["Cheatsheets"]
    lang=["Sh"]
    topics=["Shell","Tools"]
+++

## Scanning

Under Linux one can scan an image with the `scanimage` command provided by the [SANE framework](http://sane-project.org/).

{% figure(caption="A helper function that scans an image from the default scanner to the given path with settings suitable for OCR") %}
```sh
scan_image_to() {
	printf "Scanning Image …\n"
	scanimage --mode Gray --format png --resolution 300 -o "$1" -p
}
```
<b>Note:</b> The `--mode` and `--resolution` options are scanner specific and my work different for you. See `scanimage --help`.
{% end %}

### Unpaper

[Unpaper](https://github.com/unpaper/unpaper) ([manpage](https://man.voidlinux.org/unpaper)) is a scan post-processing tool which can remove paper and scan artefacts and also do alignment of carelessly scanned pages. Which is great when preparing them for OCR.

Thanks to a friend of mine for mentioning it! 

## OCR using Tesseract and imagemagick

[Tesseract](https://github.com/tesseract-ocr/tesseract) is both, a library and [command line tool](https://man.voidlinux.org/tesseract) for Optical Character Recognition (OCR). Image in, text in image out. 

It best works with greyscale images with low background noise and high contrast. Getting a scan closer to that can be done using unpaper or - for scans of known quality - imagemagick using its builtin contrast filter and the level clipping, which is faster than any fully automatic tool.

{% figure(caption="A function that can be used to OCR a given prerotated image that does some preprocessing with imagemagick to improve noise (the -level option) and contrast for scans, the output is the OCR as plaintext to stdout.") %}
```sh
# Usage: ocr_image <image>
ocr_image() {
	convert "$1" -colorspace Gray -contrast -level 20%,100% -contrast -contrast - | \
	tesseract -l deu+eng --oem 2 --psm 3 - -
}
```
{% end %}

To explain the most important tesseract options:

{% dl() %}
-l
: To set the used [language or script](https://man.voidlinux.org/tesseract#LANGUAGES_AND_SCRIPTS). Accepts multiple values separated by a `+`. i.e. `-l deu+eng`

--oem
: To set the mode of operation, meaning whether or not to use the [LSTM](https://en.wikipedia.org/wiki/Long_short-term_memory) based approach or the original tesseract OCR. At the time of writing I had the best results with option 2, enabling both.

--psm
: Tells tesseract what [kind of layout analysis](https://man.voidlinux.org/tesseract#OPTIONS) to run. I prefer to do a separate OSD (Orientation and Script Detection) only run in mode 0 rotating the image and then using the full automatic layout mode 3 without OSD to do the actual OCR.

--user-words
: To give tesseract a text file with a list of words that are likely to occur in the document, this helps avoiding the OCR equivalent to a typo. There is also a pattern file (note that these are <b>not</b> regexes!). [More on word and pattern lists in the tesseract manpage.](https://man.voidlinux.org/tesseract#CONFIG_FILES_AND_AUGMENTING_WITH_USER_DATA)

{% end %}

<b>Note for Void-Linux users:</b> The package name and command for calling tesseract is `tesseract-ocr` on Void.

{% figure(caption="This function uses tesseract with the psm 0 mode to correctly orientate a given image file that is assumed to be German or English text. The file is then replaced.") %}
```sh
# Usage: rotate_scanned_image <image>
rotate_scanned_image() {
    # Replace or leve out the -l option here if you work with other languages/scripts
	PAGE_OSD="$(tesseract -l deu+eng --oem 2 --psm 0 "$1" - 2>&1)"
    #if DEBUG_OSD is set print some debug output.
	[ -n "$DEBUG_OSD" ] && printf "PAGE OSD =====\n%s\n==============\n" "$PAGE_OSD" >&2
	APPLY_ROTATION="$( printf "%s\n" "$PAGE_OSD" | awk '/^Rotate:/{ print $2 }' )"
	convert "$1" -rotate "${APPLY_ROTATION:-0}" "$1"
}
```
{% end %}

<b>Note on DPI settings:</b> Tesseract assumes the images to be scanned at 300dpi, if you are using a different resolution make sure to tell tesseract using the `--dpi` option.

## PDF creation and manipulation

### Creating a PDF from images

To create a PDF from images without OCR one can use imagemagick.

{% figure(caption="Example that uses the <code>convert</code> command to convert two png files to an pdf with a pagesize of DIN A4.") %}
```sh
convert -page a4 page_1.png page_2.png document.pdf
```
{% end %}

Also see the [imagemagick documentation on the -page option.](https://imagemagick.org/script/command-line-options.php#page).

### OCR using OCRmyPDF

[OCRmyPDF](https://ocrmypdf.readthedocs.io/en/latest/index.html) is a python wrapper for tesseract that makes it pretty painless to run OCR on a PDF and turn PDFs into [standards compliant PDF/A](https://ocrmypdf.readthedocs.io/en/latest/introduction.html#about-pdf-a) and optimize PDFs to reduce filesize.

{% figure(caption="An example for OCRing a PDF file that contains German and English text as images.") %}
```sh
ocrmypdf -l deu+eng input.pdf output.pdf
```
{% end %}

!note: <b>Package Note:</b> The package is called `ocrmypdf` for all major Linux distributions (AUR on Arch).

Thank you (again) to the friend who found this tool.

### Creating PDFs with OCR using tesseract and poppler

To create PDFs with an OCR overlay one can delegate the image to PDF step to tesseract using its `pdf` preset and join the resulting documents after the OCR step with the poppler [`pdfunite`](https://man.voidlinux.org/pdfunite) tool.

{% figure(caption="Tesseract command to generate a PDF with OCR overlay for a single page with the options left out (they are the same as with the plaintext preset)")%}
```
tesseract page_n.png page_n [options...] pdf
```
{% end %}


{% figure(caption="pdfunite command to join multiple PDFs together. Btw. these can be any PDF documents." )%}
```sh
pdfunite page_*.pdf document.pdf
```
{% end %}

!note: <b>Accessibility Note:</b> These OCR overlays while pretty good are intended for making the documents searchable, they are not a replacement for proper semantic and accessible documents.

### Getting text out of PDF files

To get text out of a PDF one can use the [`pdftotext`](https://man.voidlinux.org/pdftotext) and [`pdftohtml`](https://man.voidlinux.org/pdftohtml) command (which come with poppler).

```sh
pdftotext file.pdf
```

## Packages

To get the commandline utilities on some distributions one should also install the `<packagename>-bin` package.

Imagemagick is sometimes just called `magick`.

Tesseract usually comes without the needed data files, these probably come in packages called like `tesseract-data-<lang>`, if you just want everything there is probably a `tesseract-data-all` metapackage.

The `pdfunite` and `pdfto*` commands are either directly in the `poppler` package or in `poppler-utils`.
