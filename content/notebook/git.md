+++
	title="Git Cheatsheet(s)"
	description="Some of my git knowledge and where it came from."
	date=2023-11-03

	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Tools"]
+++

## Setting Up

Before starting to use git one should take the few minutes to set it up.

Set up your personal information that will end up on your commits:
```sh
git config --global user.email <your-mail@example.org>
git config --global user.name "<Name>"
```

Set the default branch name to main instead of master
The reasons I do this are because there was some controversy
around the word "master" in this particular context
and and I quickly realised that "main" is way easier to type.
```sh
git config --global init.defaultBranch main
```

You could set your editor for git commit messages (and other git features) using:
```sh
git config --global core.editor <editor-command>
```

If you (like me) have one editor you want to use for everything: Set the [`EDITOR` variable in your `~/.profile`](/notebook/env#linux-and-posix) and git along with a whole range of other programs will pick up on that.

## Partial Adding

Situation: You have made changes to one file but they should be in two different commits.

```sh
git add -p [<file>]
```

git will then chop up the changes into <i>hunks</i> and ask interactively if you want to add/stage (`y`) a change, ignore/not stage it (`n`), further subdivide (`s`) or even edit (`e`) it. You can use `?` to get summary of all options currently available.

## Undoing Things

### Undo last Commit

Situation: You just made a git commit but you committed too much by accident.

<b>Note:</b> If you just messed up the commit message or need to add changes use `git commit --amend`.

`git reset` is your friend here, without any arguments it helps when you `git add`ed too much, it makes git go into the state it was in at the last commit.

```sh
git reset HEAD^1
```

The [`HEAD^1`](https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection#_ancestry_references) resets git to the parent commit of the last commit. Effectively acting as if the last commit never happened. (You can still recover it using the revlog)

`git reset` will only change your git history, not the files you have currently checked out so when using it as described above you won't loose any changes.

<b>Warning:</b> `git reset --hard` is intended to throw changes away and it *is* possible to loose uncommitted data when using it with the `--hard` flag enabled.

### Restore File from last Commit

Situation: You messed up a file and want to throw away the changes since the last commit.

```sh
git restore <filename>
```

This is less destructive than `git reset --hard` which throws away all your changes in a repository since the last commit.

## Other Peoples Git Guides

These are guides made by other people that I think are useful.

In [My unorthodox, branchless git workflow](https://drewdevault.com/2020/04/06/My-weird-branchless-git-workflow.html) Drew DeVault explains how to make use of the fact that the git history of a repository isn't set in stone, I mainly use these tricks to completely avoid a lot of merge conflicts in my personal projects.

[Learn to change history with git rebase!](https://git-rebase.io/) is Drew DeVault's cheatsheet for altering git history in general and the rebase based workflow in the blogpost liked above.

In [Dealing with diverged git branches](https://jvns.ca/blog/2024/02/01/dealing-with-diverged-git-branches/) Julia Evans explains how to deal with the situation that changes happend on both the local and remote git repository. (She explains it better than I do here.)

[Ein kurzer Blick in .git/](https://media.ccc.de/v/DiVOC-5-ein_kurzer_blick_in_git#t=0) is a talk held at DiVOC in 2020 by Florian Hars diving into the `.git` directory, unfortunately no one has translated it to English yet.

[Inside .git](https://jvns.ca/blog/2024/01/26/inside-git/) is a blogpost by Julia Evans that also dives into the `.git` directory and explains what is inside.

