+++
	title="Fontconfig"
	description="Some knowledge that makes your life easier when working with fonts on Linux."
	date=2023-06-20
	updated=2024-08-05

	[extra]
	banner="broken_font"

	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Desktop Ricing"]
+++

## What is Fontconfig?

> Fontconfig is a library for configuring and customising font access.

It is built into almost every program that runs on Linux and has a GUI (even in into old java software!) and makes it very easy to make configure fonts and then forget about it.

You can find the [Fontconfig documentation over on freedesktop.org](https://www.freedesktop.org/wiki/Software/fontconfig/).

## Fontconfig CLI Tools

You average Linux distribution ships Fontconfig along with some commands to poke it.

{% dl() %}
[fc-cache](https://man.voidlinux.org/fc-cache)
: for rebuilding the Fontconfig cache (after installing a font or changing configuration).<br>Usually used like this: `fc-cache -f -v`

[fc-conflist](https://man.voidlinux.org/fc-conflist)
: gives you a list of configuration files found by Fontconfig.

[fc-match](https://man.voidlinux.org/fc-match)
: for looking up which exact font a given name resolves to, *very* useful for debugging.

[fc-query](https://man.voidlinux.org/fc-query)
: to print out metadata for a font file.

[fc-validate](https://man.voidlinux.org/fc-validate)
: can give a hint abut missing glyphs in a font file for a given language.

[fc-cat](https://man.voidlinux.org/fc-cat)
: for having a look at the Fontconfig cache.

[fc-list](https://man.voidlinux.org/fc-list)
: to list all fonts Fontconfig knows about.

[fc-pattern](https://man.voidlinux.org/fc-pattern)
: helps you by parsing [Fontconfig patterns](https://man.voidlinux.org/FcPatternFormat) , useful for debugging.

[fc-scan](https://man.voidlinux.org/fc-scan)
: similar to `fc-query` but has the same output format as `fc-pattern`.
{% end %}

## Font (Family) Names

What one usually calls the fonts name actually is the font family name. This is because of historical reasons from back when a font was a case of lead letters for printing and one needed an extra case full of letters for the bold or italic styles.

The `family` name is usually what is accepted whenever you have some configuration file and it wants a font name. Usually the postscript name works too. Both can be found out using `fc-query` on a given font file. `fc-list` also shows the family name by default.

<b>Note:</b> `fc-query -b` omits language and character set information resulting in a more readable output.

<b>Note:</b> Fontconfig also has a [specific font name](https://www.freedesktop.org/software/fontconfig/fontconfig-user.html#AEN36) that includes the family name, size, style and every other possible font setting.

## System Fonts

System fonts using Fontconfig are just configuration aliasing generic names to your favourite font.

The most common are `monopace`, `sans` or `sans-serif` and `serif`. If your desktop environment has a font preference dialog in the system settings it is changing which font names those aliases point to.

<b>Note:</b> There is also a convention to alias `emoji` to your preferred emoji font.

## Finding Fonts that Support a Codepoint

To query fonts that support a given character, you need to know its codepoint.

For the following examples I'll use the 😃 emoji which has the codepoint `U+1f603`.

If you have a unicode aware `printf` command (if you don't know simply try):
```sh
printf %x "'😃'"
```

Which outputs `1f603`.

There is also the possibility to use the `uni` command.

```sh
uni identify 😃
```

It also lists `U+1F603`.

{% chat() %}
: /resources/emoji/neofox/neofox_laptop.png ::: Less techy neofox says:
> You could also look it up using a search engine, Wikipedia or one of those Emoji lookup sites.
{% end %}

---

You have the codepoint? Great, now you can ask the `fc-list` command (replace the example `1f603` with the codepoint you are interested in.)

```sh
fc-list :charset=1f603
```

You should get a list of all fonts on your system that support that codepoint, if that codepoint isn't supported by a font on your system the output stays empty.

## Configuration Files

You can find your Fontconfig configuration file in `$XDG_CONFIG_DIR/fontconfig` which in most cases is `~/.config/fontconfig/`. In there you can either directly edit the `fonts.conf` file or a file in the `conf.d` folder.

Global Configuration is in `/etc/fonts/`. It usually consists of files linked from `/usr/share/fontconfig/` where configuration snippets for common settings are installed.


### Using Predefined Configuration

fc-conflist will list all configuration files for fontconfig, the active ones prefixed with `+` the inactive ones in `/usr/share/fontconfig/` prefixed with a `-`.

To use the predefined configuration symlink it:
```sh
ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/
# Regenerate the fonconfig cache
fc-cache -fv
```

<b>Note:</b> The [Void Linux Documentation on Fonts](https://docs.voidlinux.org/config/graphical-session/fonts.html) gives a similar example on linking the `70-no-bitmaps.conf` to `/etc/fonts/conf.d/` to [mitigate the common issue that Firefox picks pixelated bitmap fonts](https://github.com/void-linux/void-packages/issues/22477) over higher quality ones.

### Writing Custom Configuration

To make a valid Fontconfig configuration file it needs some [XML scaffolding](https://www.freedesktop.org/software/fontconfig/fontconfig-user.html#AEN53):

```xml
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "urn:fontconfig:fonts.dtd">
<fontconfig>
	<!-- actual configuration goes here -->
</fontconfig>
```

That scaffold can now be filled with some rules.

<b>Note:</b> In case you are in need of an example, have a look at the [Fontconfig documentation examples](https://www.freedesktop.org/software/fontconfig/fontconfig-user.html#AEN252) or [my Fontconfig](https://codeberg.org/slatian/dotfiles/src/branch/main/.config/fontconfig).


Those rule could be disabling fonts without having to uninstall them, useful when one doesn't have root access or wants to keep the font files for some reason.

{% figure(caption="Example rule for disabling FontAwesome to prevent it from interfering with other icon-fonts.")%}
```xml
<selectfont>
	<rejectfont>
		<pattern>
			<patelt name="family"><string>FontAwesome</string></patelt>
		</pattern>
		<!-- More pattern instances for other fonts can go here -->
	</rejectfont>
</selectfont>
```
{% end %}

Another application is to configure the system fonts mentioned before using `alias` rules. For setting the `emoji` (or any other) font one simply maps the family name `emoji` to the family name of the preferred font.

{% figure(caption="An example rule for aliasing <code>emoji</code> to <code>Twemoji</code>.", caption_after="The same mechanism could be used to alias something like <code>Helvetica</code> to your favourite sans font family.") %}
```xml
<alias>
	<family>emoji</family>
	<prefer>
		<family>Twemoji</family>
	</prefer>
</alias>
```
{% end %}

The `prefer` part of the alias also accepts multiple font families, this can be used to apply the `emoji` alias to system fonts by telling Fontconfig to prefer your favourite monospace, sans or serif font <b>and</b> whatever is aliased to `emoji` over all other installed fonts.

```xml
<alias>
	<family>monospace</family>
	<prefer>
		<family>Noto Sans Mono</family>
		<family>emoji</family>
	</prefer>
</alias>
```

Of course there are more applications for Fontconfig configurations, but those probably deserve more specific pages.

<b>Note:</b> I'm using Twemoji and Noto Sans, not because I like the companies that made them, but because they are under a permissive license are mostly complete, look okay and are one of the very few things those companies are actually doing good for humanity.

There are community made fonts like [Inter](https://rsms.me/inter/) and [OpenMoji](https://openmoji.org/) (plus some more) out there with an impressive coverage and feature set that shouldn't go unmentioned here.

## Installing fonts

### From packages

Usually the packages do everything for you and even install some sane Fontconfig rules.

If they don't, have a look at the Fontconfig files they install and check if they clash with your own configuration or if the package description, documentation or comment in its Fontconfig file mentions additional required configuration. This is usually the case for emoji fonts to get them as the default font.

### From files

In case you have a modern Linux distribution you can install a font by dropping the ttf or otf file into `~/.local/share/fonts/` or `~/.fonts/`, the latter works on older distributions too.

To apply the changes run `fc-cache -f -v` to rebuild the Fontconfig cache and restart your applications (or log out and back in).

Before restarting applications you could verify that the font was installed using the `fc-match` and `fc-list` commands.

#### Converting woff2 to ttf

Google (who else 🙄) made a [tool for converting woff2 fonts to ttf and back](https://github.com/google/woff2), the package containing it is simply called `woff2` in most Linux distributions.

It contains the `woff2_decompress` command, give it a path to a woff2 font file and it will convert it to ttf. There is also a `woff2_compress` that works in the other direction.

