+++
	title="XML XPath"
	description="Query elements from XML Documents, on the CLI, in the Browser, everywhere else"
	date="2023-12-22"
	[taxonomies]
	notebook=["Cheatsheets"]
	lang=["XML"]
+++

XPath is a way to select tags from an XML Document, it works a bit like a File-path and a bit like a CSS-Selector.

## Example Document

For the Examples on this page the following XML-Document is assumed.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<catalog>
	<title>An Overview of Dataformats.</title>
	<use>Describes Dataformats</use>
	<item>
		<name>JSON</name>
		<use>Data Serialization</use>
		<link type="website" href="https://www.json.org" />
		<link type="wikipedia" href="https://en.wikipedia.org/wiki/JSON" />
	</item>
	<item>
		<name>XML</name>
		<use>Documents</use>
		<link type="website" href="https://www.xml.com/" />
		<link type="wikipedia" href="https://en.wikipedia.org/wiki/XML" />
	</item>
</catalog>
```

{% linkbutton(href="./xpath-example.xml", icon="action-download", download=true) %}Download Example XML{% end %}

## Querying Nodes

XPath works like a file-path.

<b>Example:</b> To address the `title` tag (<i>node</i> in XPath terminology).
```
/catalog/title
```

<b>Note:</b> If you are inside a context (i.e. one of the `item` elements) you can also use it like a relative file-path: `use`, `./name`, `../title`

For wildcards on a single level one can use a `*` for the tag name.

<b>Example:</b> Same as previous but with a wildcard.
```
/*/title
```

By using `//` one can select all elements matching the following description no matter where in the document they are.

<b>Example:</b> Select *all* three `use` tags.
```
//use
```

<b>Note:</b> Using the `//` like `/catalog//use` will also work.

## Querying Attributes

To get the attribute of a tag use the notation `/path/to/tag/@attribute`.

<b>Example:</b> To get the `href` attributes:
```
//link/@href
```

## Dealing with Multiple Elements using Predicates

When specifying an XPath that yields multiple results maybe one wants not all elements that match a tag.

Writing a number in square brackets will work similar to an array access in a programming language: `tag[1]`

<b>Example:</b> Select the second `item` from the catalog:
```
/catalog/item[2]
```

In place of the Number on can also use expressions like `last()`, `last()-1` or `position()>1`.

One can also compare against attributes here.

<b>Example:</b> Only select the `type="wikipedia"` links.
```
//link[@type='wikipedia']
```

<b>Note on Quoting:</b> XPath uses single quotes so one can easily use it in XML attributes.

!note: <b>Boolean Logic:</b> Boolean logic is implemented with the `and` and `or` keywords and the `not()` function.

These are way more possibilities, the [w3schools tutorial has a more complete predicate list](https://www.w3schools.com/xml/xpath_syntax.asp).

## Getting Node Text

To get the text inside a node use `/text()` to select it.

<b>Example:</b> Get the text from the first items name:
```
//item[1]/name/text()
```

## Getting Attribute Text

To get the text from an attribute one can prefix the attribute name with an `@`.

<b>Example:</b> To get all URLs from the link elements from the example document.
```
//link/@href
```

<b>Note:</b> This will select the attribute as a key-value pair in some contexts. To only get the value wrap it like this: `concat('',//link/@href)` (only keeps first element) or `string-join(//link/@href,' ')` (keeps all elements but requires XPath 2 support).

{% figure(caption="Example using <code>xquilla</code> to output all link URLs from the example document using newlines as delimiters. The <code>printf</code> is used to convert the <code>\n</code> to a real newline.") %}
```sh
printf "string-join(//link/@href,'\n')" |
xqilla /dev/stdin -p -i xpath-example.xml 
```
{% end %}

## Combine multiple XPaths

Sometimes the result of one XPath isn't enough and you want the combined results of multiple XPaths. (Like a SQL union)

To achieve this you join multiple XPaths using a pipe `|` character like this: ` /xpath1 | /xpath1`

## XPath with Namespaces

If some of your Nodes are part of a namespace, i.e. using the `<namespace:tag>` syntax or the `<tag xmlns="https://example.org">` attribute

<b>Note:</b> When using the `xmlns` attribute all children of that node are also in that namespace unless declared otherwise.

When a namespace is used you'll notice that your usual queries don't work for some strange reason.

{% figure(caption="Snippet of Atom-Feed serving as an example documents here.")%}
```xml
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
    <title>slatecave.net</title>
    <!-- Rest of feed -->
</feed>
```
{% end %}

To get the title one may want to try the XPath `/feed/title`, but this will fail because of the namespace.

If it is possible to declare the namespace (i.e. in an XSLT-Sheet) then do that and select the element with the namespace prefix, i.e. `/atom:feed/atom:title`.

If it is not possible to declare the Namespace you can work around that by using the `local-name()` function like this: `*[local-name()='feed']/*[local-name()='title']`

## Useful Links

All of the following lead to the w3schools page.

{% linklist(icon="destination-text") %}
* [XPath Syntax](https://www.w3schools.com/xml/xpath_syntax.asp)
* [XPath Operators](https://www.w3schools.com/xml/xpath_operators.asp)
* [XPath Functions](https://www.w3schools.com/xml/xsl_functions.asp)
* [XPath Tutorial](https://www.w3schools.com/xml/xpath_intro.asp)
{% end %}

<b>Note:</b> There is also the related [RFC 9535: JSONPath: Query Expressions for JSON](https://www.rfc-editor.org/info/rfc9535) that might be interesting.

## Playing with XPath

### Using `xmllint`

The [`xmllint`](https://man.voidlinux.org/xmllint) command line utility can do XPath.

```sh
xmllint <file.xml> --xpath "<xpath>"
```

<b>Note:</b> `xmllint` and XPath are the `jq` of XML, if they seem a bit clunky that is because they have been around for a freaking long time.

!note: <b>Compatibility:</b> `xmllint` only works with XPath 1.0, if you get an error stating that a function is unregistred, thats why. It mostly affects functions that somehow involve lists.

### Using `xquilla`

The [`xquilla`](https://manpages.org/xqilla) command line tool supports XPath 2.0 and a whole lot of other XML functionality. It usually reads commands from a file and applies it to an XML-Document.

```sh
echo "<xpath>" | xquilla /dev/stdin -p -i <file.xml>
```

### Getting information from HTML

<b>Example:</b> Get the title of the HTML Document.
```sh
curl https://slatecave.net | \
xmllint - --html --xpath "/html/head/title/text()" 2>&-
```

<b>Example:</b> Get the social media preview description.
```sh
curl https://slatecave.net | \
xmllint - --html --xpath \
	"string(
	/html/head/meta[@property='og:description']/@content |
	/html/head/meta[@property='og:description']/text()
	)" 2>&-
```

<b>Explanation:</b> Both examples use `-` to tell `xmllint` to read from its standard input (that is the `curl` output here), enable the HTML parser with the `--html` option and discard any errors from `xmllint` using `2>&-`, which tells the shell to close the standard error.
