+++
	title="Speech Recognition Frontends"
	description="Projects making use of speech recognition"
	date=2023-05-09
	[taxonomies]
	notebook=["Bookmarks"]
+++

This page intends to give an overview what kind of projects out there are open source (or preferably free software) making use of speech recognition without relying on some well known cloud providers.

## Overview

The projects listed here fall into 3 broader categories.

Build your own voice something:
* [voice2json](#voice2json)
* [Dragonfly](#dragonfly)

Voice Input Methods that could replace Mouse and Keyboard:
* [Numen](#numen)
* [Simon](#simon-kde)

Voice assistants:
* [Rhasspy](#rhasspy-voice-assistant)
* [Mycroft](#mycroft-voice-assistant)

## voice2json

> `voice2json` is a collection of [command-line tools](https://voice2json.org/commands.html) for offline speech/intent recognition on Linux. It is free, open source ([MIT](https://opensource.org/licenses/MIT)), and [supports 18 human languages](https://voice2json.org/#supported-languages).

{{ linkbutton(href="https://voice2json.org/") }}

It can make use of [CMU Sphinx](https://kaldi-asr.org/), [Kaldi](https://kaldi-asr.org/), [Mozilla DeepSpeech](https://github.com/mozilla/DeepSpeech) and [Julius](https://github.com/julius-speech/julius), depending on the used language profile.

## Dragonfly

> Dragonfly is a speech recognition framework for Python that makes it convenient to create custom commands to use with speech recognition software.

{{ linkbutton(href="https://github.com/dictation-toolbox/dragonfly") }}

I learned about this from [handsfree.dev](https://handsfree.dev).

## Numen

> Numen is voice control for computing without a keyboard. It works system-wide on Linux and the speech recognition runs locally.

{{ linkbutton(href="https://numenvoice.org/") }}

[John Gebbie](https://johngebbie.com/) also has some [related projects linked in the README](https://git.sr.ht/~geb/numen#see-also), he also made [handsfree.dev](https://handsfree.dev/).

## Simon (KDE)

> Simon is an open source speech recognition program that can replace your mouse and keyboard. The system is designed to be as flexible as possible and will work with any language or dialect.
>
> Simon uses the [KDE libraries](https://kde.org), [CMU SPHINX](https://cmusphinx.github.io/) and / or [Julius](https://julius.osdn.jp/en_index.php) coupled with the [HTK](https://htk.eng.cam.ac.uk/) and runs on Windows and Linux.

{{ linkbutton(href="https://simon.kde.org/") }}

<b>Note:</b>Some links have been replaced with their redirect targets.

## Rhasspy Voice Assistant

> Rhasspy (ɹˈæspi) is an [open source](https://github.com/rhasspy), fully offline set of [voice assistant services](https://rhasspy.readthedocs.io/en/latest/#services) for many human languages […]

{{ linkbutton(href="https://rhasspy.readthedocs.io") }}

## Mycroft Voice Assistant

Mycroft is an open source voice Assistant that makes use of a [custom wakeword listener](https://github.com/MycroftAI/mycroft-precise) and Mozilla [DeepSpeech](https://github.com/mozilla/DeepSpeech) together with some custom components that has no easy-to-find non-marketing short description.

If you are looking for the technical stuff instead of company selling things stuff, you'll find it in the Documentation menu of their page.

{{ linkbutton(href="https://mycroft.ai/") }}

## Feedback

If some of the information here is outdated, incorrect or you know a project that is not in this list please [open an issue over on Codeberg](https://codeberg.org/slatian/site-source.slatecave-net/issues) or [contact me in some other way](/about/me#contact).
