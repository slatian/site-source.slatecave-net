+++
	title="Resistor Value Table"
	description="For when those Rings don't make sense again."
	date="2023-01-14"
	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Electronics and Microcontroller"]
+++

## How To Read Resistors

<table>
<tbody>
<tr><th>Color</th><th>Value</th><th>Multiplier</th><th>Tolerance</th></tr>
<tr class="color-row black"><td>Black</td><td>0</td><td>1Ω</td><td></td></tr>
<tr class="color-row brown"><td>Brown</td><td>1</td><td>10Ω</td><td>± 1%</td></tr>
<tr class="color-row red"><td>Red</td><td>2</td><td>100Ω</td><td>± 2%</td></tr>
<tr class="color-row orange"><td>Orange</td><td>3</td><td>1KΩ</td><td></td></tr>
<tr class="color-row yellow"><td>Yellow</td><td>4</td><td>10KΩ</td><td></td></tr>
<tr class="color-row green"><td>Green</td><td>5</td><td>100KΩ</td><td>± 0.5%</td></tr>
<tr class="color-row blue"><td>Blue</td><td>6</td><td>1MΩ</td><td>± 0.25%</td></tr>
<tr class="color-row violet"><td>Violet</td><td>7</td><td>10MΩ</td><td>± 0.10%</td></tr>
<tr class="color-row gray"><td>Gray</td><td>8</td><td></td><td>± 0.05%</td></tr>
<tr class="color-row white"><td>White</td><td>9</td><td></td><td></td></tr>
<tr class="color-row gold"><td>Gold</td><td></td><td>0.1Ω</td><td>± 5%</td></tr>
<tr class="color-row silver"><td>Silver</td><td></td><td>0.01Ω</td><td>± 10%</td></tr>
</tbody>
</table>

Resistors are usually marked with 4 or 5 colored rings to indicate value and tolerance. To read the code in the right direction you have to know that the order is:

1st Ring, 2nd Ring, 3rd Ring (only when you have a 5-Ring code), Multiplier Ring, a larger gap (sometimes not), Tolerance Ring

To decode, read the 1st to 3rd Ring as if they were decimals and then multiply that number with the Multiplier.

For SMD resistors the pattern apparently repeats itself where the last digit on the package is a Multiplier for the other digits, resulting in a value in ohms. Also the form where there is an `R` in place of the decimal point is common for smaller values.
