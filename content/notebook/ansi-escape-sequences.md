+++
	title = "ANSI Escape Sequences"
	description = "The things that make your Terminal colourful"
	date = "2023-05-28"
	updated = 2024-04-21

	[taxonomies]
	notebook=["Cheatsheets"]
	topics=["Shell"]
+++


[ANSI Escape sequences](https://en.wikipedia.org/wiki/ANSI_escape_code) are some standardised byte sequences that make most Terminals do some thing other than just printing characters on screen. Like printing colourful characters!

## Using ANSI-Escape Sequences

ANSI-Escape sequences can be used by writing an ASCII-Escape characters followed by some instructions to your Programs standard output or standard error.

The codes can be produced by any string escaping mechanism. In proper programming and scripting languages the syntax for the escape character in double quoted strings is usually `\x1b`.

The shell being a bit different you should use either `printf` or `echo -e`. While most systems support both the chance of `printf` being supported is usually higher.

## Using ANSI-Escape Sequences Responsibly

!warning: <b>While colors and text effects are pretty:</b> You should consider the cases where they shouldn't be enabled and respect user choices.

* Disable color output by default when the [`NO_COLOR` environment variable is non-empty](https://no-color.org).
* Disable use of escape codes by default when the output isn't a terminal.
* Don't overuse color, it gets confusing pretty quickly.
* Make sure no information gets lost when disabling escape codes.
	* Does using grep on the output work to find information?
	* Does piping it into `espeak-ng` give a sensible result?

When writing <q>by default</q> that means unless **explicitly** requested (i.e. through a `--color always` CLI option).

{% linkbutton(href="https://seirdy.one/posts/2022/06/10/cli-best-practices/", icon="destination-text") %}Seirdy about <q>Best practices for inclusive CLIs</q>.{% end %}

!note: <b>Thank you to Seirdy!</b> Most of the above is a summarized and filtered TL;DR of Seirdys article, you should definitely read it!

## Setting Text Color and Effects

I'll focus on Select Graphic Rendition (SGR) codes here, [Wikipedia has a more complete list](https://en.wikipedia.org/wiki/ANSI_escape_code#CSIsection).

{% figure(caption="An example for the shell which prints out <q>_FOOBAR_</q> in bright (<code>01</code>) red (<code>31</code>). After that, it resets to default settings (<code>00</code>) and prints a newline character (<code>\n</code>)") %}
```sh
printf '\033[01;31m_FOOBAR_\033[00m\n'
# With %s placeholder
printf '\033[01;31m%s\033[00m\n' "_FOOBAR_"
```
{% end %}

As you can see in the example, multiple escape-codes can be chained in one sequence by delimiting them with a `;`.

The `\033` part will turn into an ASCII escape character, the `[` tells the terminal that this is an ANSI Control Sequence Initiator (CSI) after that can come one or more codes. The sequence is finished by an `m` to tell the terminal that everything between the <abbr title="Control Sequence Initiator">CSI</abbr> and it are <abbr title="Select Graphic Rendition">SGR</abbr> codes.

<b>Note on the `\033` and compatibility:</b> The `\033` is the octal representation of the Escape ASCII-Character and is the most widely supported one across `printf` implementations. (The dash printf implementation for example doesn't support hexadecimal representation.)

<table>
<caption>Most commonly used codes</caption>
<tr>
<th>Code</th>
<th>Code to reverse</th>
<th>Effect</th>
</tr>
<tr><td>00</td><td></td><td>Reset all effects</td></tr>
<tr><td colspan="3" class="table-subheading">Foreground color</td></tr>
<tr><td>30-37</td><td>39</td><td>Set the Foreground color</td></tr>
<tr><td>90-97</td><td>39</td><td>Set the Foreground color, bright variation</td></tr>
<tr><td colspan="3" class="table-subheading">Background color</td></tr>
<tr><td>40-47</td><td>49</td><td>Set the Background color</td></tr>
<tr><td>100-107</td><td>49</td><td>Set the Background color, bright variation</td></tr>
<tr><td colspan="3" class="table-subheading">Font effects</td></tr>
<tr><td>01</td><td>22</td><td>Bright text (usually bold and brighter color)</td></tr>
<tr><td>02</td><td>22</td><td>Dim text (usually only effect on color)</td></tr>
<tr><td>03</td><td>23</td><td>Italic text</td></tr>
<tr><td>04</td><td>24</td><td>Underlined text</td></tr>
<tr><td>05</td><td>25</td><td>Blinking text</td></tr>
<tr><td>07</td><td>27</td><td>Reverse foreground and background color</td></tr>
<tr><td>08</td><td>28</td><td>Concealed/Hidden text</td></tr>
<tr><td>09</td><td>29</td><td>Strikethrough text</td></tr>
</table>

<b>Note:</b> The leading zeros are not needed.

### Colors

You may have noticed that the `3x`, `4x`, `9x`, and `10x` family of codes all use the last digit in the range from 0 to 7 as a color parameter.

<table>
<caption>Table of colors</caption>
<tr><th>Code</th><th>Color</th></tr>
<tr><td>0</td><td>Black or Dark Grey</td></tr>
<tr><td>1</td><td>Red</td></tr>
<tr><td>2</td><td>Green</td></tr>
<tr><td>3</td><td>Yellow</td></tr>
<tr><td>4</td><td>Blue</td></tr>
<tr><td>5</td><td>Violet / Purple</td></tr>
<tr><td>6</td><td>Cyan or Turquoise</td></tr>
<tr><td>7</td><td>Light Grey or White</td></tr>
</table>

Please keep in mind that different people have different color palettes when you want to write reusable scripts.

