+++
	title = "Interesting Resources for building Search Engines"
	description = "A collection of links to articles and documentation."
	date = 2025-02-01

	[taxonomies]
	notebook = ["Bookmarks"]
	topics = ["Web"]
+++

## How Search works

Articles that dive into how a Search Engine works under the hood.

{% linklist() %}
* [How Search Engines Work — Summa Blog](https://izihawa.github.io/summa/blog/how-search-engines-work/)
* [How to […]: inside a full text search engine — meilisearch Blog](https://www.meilisearch.com/blog/how-full-text-search-engines-work)
* [The Anatomy of a Large-Scale Hypertextual Web Search Engine — The original Google Paper (PDF)](http://infolab.stanford.edu/pub/papers/google.pdf)
{% end %}

## Datastructures

Datastructures useful for building search engines.

### FST — Finite State Transducers

In a nutshell: FSTs use properties of [State Machines](https://en.wikipedia.org/wiki/Finite-state_machine) to store sorted sets of strings, map strings to numbers and run fast prefix queries, but also other queries using automata like regex or even [levenstein distance](https://en.wikipedia.org/wiki/Levenshtein_distance) queries.

{% linkbutton(href="https://burntsushi.net/transducers/") %}Index 1,600,000,000 Keys with Automata and Rust {% end %}

An implementation would be the [rust fst crate](https://github.com/BurntSushi/fst).

### Roaring Bitmaps

[Roaring Bitmaps](https://roaringbitmap.org/) are datastructures, that from the outside can be operated like giant bitmaps, storing very big sets of numbers, but optimise themselves on the inside to use memory as efficient as possible and staying fast.

{% linkbutton(href="https://arxiv.org/pdf/1402.6407v4") %}Better bitmap performance with Roaring bitmaps — Research Paper (PDF){% end %}

## Crawling strategies

### Stract

Stract is mostly Interesting because of its politeness algorithm that in a nutshell is a politeness factor multiplied by a either the response time or a minimum delay, whichever is larger. Capped at a maximum delay of 180s that should be good for the website and the crawler.

<b>Note:</b> While the algorithm is Interesting the robots.txt `Crawl-delay` should take priority if present (though that one should also be capped at a friendly value).

{% linkbutton(href="https://stract.com/webmasters")%}Stract Crawler — Webmaster Documentation{% end %}

## Server to Crawler Protocols

If are crawling the web and care about admins not making your life difficult one should respect the web servers and their administrators. Respecting these signals also helps gathering higher quality data.

The baseline is implementing and respecting robot.txt, other protocols may provide additional useful metadata depending on your use case.

{% linklist() %}
* [robots.txt website](https://www.robotstxt.org/)
* [Text Data Mining — TDM Reservation Protocol](https://www.w3.org/community/reports/tdmrep/CG-FINAL-tdmrep-20240202/#sec-introduction)
{% end %}

An interesting overview is ["How I block bots" by Seirdy](https://seirdy.one/meta/scrapers-i-block/#how-i-block-bots).

## Relevancy and Ranking

List of algorithms that allow one to sort search results.

### Bag of words ranking

The most straight forward way of measuring how well a document matches a query is word counting. Though simple counters aren't very good at this, weighting them relative to statistics of your document corpus makes very good at keyword based search though.

Usually the go to is BM25 as it automatically lightens the impact of common words and rewards completeness over repeating the same words over and over again. It also has a theoretical maximum output, so one can normalise it.

{% linklist() %}
* [Okapi BM25 — simple normalised and tuneable term frequency based algorithm](https://en.wikipedia.org/wiki/Okapi_BM25)
* [TF-IDF — term frequency-inverse document frequency](https://en.wikipedia.org/wiki/Tf%E2%80%93idf)
{% end %}

### PageRank

[PageRank](https://en.wikipedia.org/wiki/PageRank) at its core is for measuring a documents overall relevancy in the context of other documents that can link to each other, not towards a search query.

It's famous for being at the core of Google for many years.

The PageRank algorithm is described in the [original Google Paper from 1998](http://infolab.stanford.edu/pub/papers/google.pdf) but also has its own dedicated Paper.

In a nutshell: Every page has a score, it distributes that score via its links to other pages (and keeps some for itself for dampening reasons) iterate a few times and the scores now represent how relevant in terms of incoming links a page is.

{% linkbutton(href="http://ilpubs.stanford.edu:8090/422/1/1999-66.pdf") %}The PageRank Citation Ranking: Bringing Order to the Web{% end %}

### TextRank

TextRank is a variation of PageRank applied to texts.

It can be used for keyword extraction, simple, automatic summaries, but also for augmenting bag of words algorithms with some smarter weights.

TextRank can also be used to pull in knowledge from a knowledge graph when connecting words and sentences.

{% linkbutton(href="https://aclanthology.org/W04-3252/") %}TextRank: Bringing Order into Text{% end %}

## Text Processing

Text by itself is pretty messy, when building a search engine one probably wants to apply some normalization.

### Snowball Stemming

>  Snowball is a small string processing language for creating stemming algorithms for use in Information Retrieval, plus a collection of stemming algorithms implemented using it.

The best thing about Snowball is that it can be compiled to other languages you are more likely to write your search engine in.

{% linkbutton(href="https://snowballstem.org/") %}Snowball{% end %}
