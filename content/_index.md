+++
	title = "Slatians hideout on the Web"
	template = "index.html"
	description = "May contain useful information in the form of Blogposts and a small personal wiki."
	generate_feeds = true
	[extra]
	show_feed_link = true
+++

<!--blank-->

## A few bytes about me …

<!--H-CARD-SECTION-->
{% chat() %}
: /resources/emoji/slatian_bean/slatian_bean.png ::: Slatian says
> Greetings, I'm <a class="p-nickname u-url" href="/about/me">Slatian</a> (sleɪtiːan) a fluffy dragon who creates Software, makes it run and breaks it (not neccessarly in that order).
>
> My interests include <a href="/lang">taking <span class="p-category">code</span> apart and putting it back together</a>, customizing my <span class="p-category">desktop</span> and <span class="p-category">terminal</span> envoirnment, the <span class="p-category">sematic web</span> and organizing information (apparantly called <a href="https://en.wikipedia.org/wiki/Ontology_(computer_science)" class="p-category">Ontology</a>). Sometimes a bit of philosophy.
> 
>/me likes opinioated software and uses Linux distributions like [Void](https://voidlinux.org/), [Artix](https://artixlinux.org/) and [Alpine](https://www.alpinelinux.org/). My favorite text editor is [kakoune](https://kakoune.org).

> You can talk to me in English or German.
{% end %}

### Me elsewhere on the internet:
{% dl() %}
Fediverse
: <a class="u-url" rel="me" href="https://pleroma.envs.net/slatian">@slatian@pleroma.envs.net</a> (<a class="u-url" rel="me" href="https://fedi.absturztau.be/baschdel">fedi backup</a>)
Git
: <a class="u-url" rel="me" href="https://codeberg.org/slatian">codeberg.org/slatian</a>
E-Mail
: baschdel ädd disroot döt org
Matrix
: [@slatian:slatecave.net](https://matrix.to/#/@slatian:slatecave.net)
{% end %}

## Explore the Slatecave!

Besides visiting my [Creations](creations), [Blog](/blog) and [Notebook](/notebook) pages …

{% linklist(icon="destination-listing") %}
* [The slatecave organized by Topic](/topics)
* [Organized by Language <small>(That is programming, markup, query and serialization)</small>](/lang)
{% end %}

Or visit some other awesome sites:

{% linkbutton(href="@/railstation/index.md", icon="destination-listing") %}The Slatecave Railstation{% end %}
