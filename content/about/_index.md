+++
	title="About slatecave.net"
	description="A bit of information about this website"
	template="page.html"
+++

## About this Website

This is a static website that uses [Zola as a site generator](https://www.getzola.org/) and is currently hosted at [netcup](https://www.netcup.de). It contains no uneccessary `div`s, `span`s and no JavaScript, the CSS is 100% handwritten by myself and it attempts to use semantic html wherever possible.

This website does not generate any income and is used for personal purposes only.

### The light mode doesn't look like a slatecave at all/is ugly?

The light mode exists because I want other to offer me my preferred colour palette, too. Also there are plenty of reasons why someone would want a light mode. So I attempted to make one while trying to minimise the use of eye-burn white. Good news is that because this website doesn't rely on black CSS + div magic you can load in almost any off the shelf stylesheet you want or use your favourite reader mode!

### Testing

I usually test this site with recent versions of

{% dl() %}
Firefox
: The browser I use for almost all my webbrowsing anyway.
Chromium
: Open source chrome with still too much google crap builtin.
Dillo (not anymore)
: A small simple and fast browser with limited CSS capabilities. The project has died, unfortunately.
Lynx
: Is a browser for the Terminal that only speaks html and is really fast (perfect for almost non-existent mobile internet), its also <em>very</em> good at visualising markup mistakes.
w3m
: Another terminal based browser, also very fast and good at finding broke markup.
{% end %}

### You are wasting how much on images !?

I know, I'm not proud of it but I like some decoration on my site and I'm trying to get the images smaller. Also I'm trying to keep the uncompressed size below 100KB and make use of your Browsers cache.

## Privacy

This site doesn't use any client site tracking technology. Cookies are used for some services hosted on `slatecave.net` to persist user preferences, these cookies can't be used to identify individual clients.

Server logs which include timestamps, request details (visited pages), IP-Address and User-Agent and reponse metadata are kept for up to 10 days for the purpose of generating visitor statistics, diagnosing technical and security issues and analyzing automated crawlers.

Data related to User-Agents that clearly identify as Robots may be kept for longer.

`slatecave.net` services run on a Server rented from Netcup GmbH and are physically located in Austria.

Visitor statistics are kept as aggregates that do not contain personally identifiable information.

No third parties are involved int the processing of personal data unless neccessary for functionality.

<b>Note:</b> Currently the only place third party services are involved is the DNS resolution of [echoip.slatecave.net](https://echoip.slatecave.net). Requests are proxied through `slatecave.net` and are by default routed to the Netcup name servers. Transmitted information includes your IP-Address and adresses explicitly requested for lookup. The inforamtion in transmitterd for the sole purpose of retrieving the corresponding DNS records.

## Legal Foo

&copy; Slatian 2024

Unless stated otherwise text on this site is licensed under a <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0 International license</a>.

Material Icons are under the <a href="https://www.apache.org/licenses/LICENSE-2.0.html">Apache License, Version 2.0</a>.

Neofox Emoji (used on some pages) [made by Volpeon licensed under CC BY-NC-SA 4.0](https://volpeon.ink/emojis/neofox/).


All opinions listet here are my own and are not representitive of my employers.

This site does <b>not</b> generate any income.

Links are placed in the hope that they'll be useful, they are not placed in exchange for a compensation. Given the nature of how the web works I'm not responsible for content outside my control, even if I link to it.

<b>Note in external links:</b> Just because I'm not responsible for linked content doesn't mean I like linking to sites that have gone bad. If you find a link to a site that has a wiered smell please [contact me](/about/me#contact) so I can fix it.
