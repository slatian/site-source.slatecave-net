+++
	title="Slatecave Atom Feeds"
	description="Subscribe to the Slatecave using your favourite feedreader."
+++

## Available Feeds

You can subscribe to …

{% linklist(icon="destination-feed") %}
* [… the whole Site](/atom.xml)
* [… my Blog](/blog/atom.xml)
* [… my Notebook](/notebook/atom.xml)
{% end %}

I'd recommend a fetch interval of once per week, once per day at maximum, feel free to fetch more or less often.

## Feedreaders

In order to subscribe you need an RSS-Reader, copy one of the links from above and add them as feeds. New content will show up in the reader when I post something new or a page updates, most readers also have the option to send notifications if desired.

<b>Note:</b> Technically the above are links to [Atom feeds](https://en.wikipedia.org/wiki/Atom_(web_standard)), but every RSS-Reader I know of also supports Atom as it is the de-facto accepted successor to RSS.

### What is a Feedreader?

A Feedreader is like a news-app, but what shows up there are articles from RSS and Atom-Feeds you subscribed, usually without any advertising.

### What Feedreader should I use?

Though to recommend one, if you already use Thunderbird you can use that to start out.

Feedreaders are available as standalone apps, browser add-ons, integrated into E-Mail clients or as (self hostable) services.

Pick one that seems reasonable for you and go from there.

<b>Note:</b>
Make sure that your feedreader can export OPML, this way you can take the subscription list with you when switching readers.

### What Feedreader are you using?

I'm using [Miniflux](https://miniflux.app) that is hosted on an old laptop in my basement.

In case you want to host it yourself too: The only real requirement is a running postgres database, almost everything else is taken care of by Miniflux after initial setup, I'd recommend using a packaged version so that Miniflux simply updates with the rest of your system.
