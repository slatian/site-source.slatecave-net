+++
	title="Slatecave Widget Zoo"
	description="A testpage that should contain all the slatecave widgets for testing."
	updated=2024-08-09
+++

## Section

This is a section with some text inside.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus feugiat dolor ac leo auctor, in placerat risus tincidunt. Sed vehicula dolor a magna dignissim placerat. Aenean libero urna, sodales sed arcu at, tincidunt scelerisque dolor.

Pellentesque vestibulum quis neque eget blandit. Aenean nec mauris lobortis, aliquet lacus eget, suscipit diam. [Nulla ultricies](#), tortor sed convallis `vehicula`, purus nulla posuere arcu, sed sodales tortor dui id ipsum. Aliquam dapibus, ex vel sodales pretium, nulla sem pulvinar nunc, sit amet mollis ante elit eu diam.

Just for testing key styling: <kbd>Ctrl</kbd>+<kbd>C</kbd>.

* This is an unordered list
* with some items in it
* and some more [and a link](#)!
	* oh and it also has a sublist
	* with more than one item.
* end of list

Proin imperdiet elementum luctus. Aliquam erat volutpat.

1. Number one.
2. Number `2`.
3. Press <kbd>3</kbd>
3. This one is *important*!

### Definition Lists!

I just <b>love</b> definition lists!

{% dl() %}
Definition
: List

Lorem
: [Ipsum](#)

Dolor
: `sid amet`
{% end %}

Usually there is some text here that explains what the mapping is all about.

{% dl() %}
One
: to many
: definitions

Lorem
: [Ipsum](#)

Dolor
: `sid amet`
: `Nunquam latinuum`
{% end %}

## Notes, Figures, Blocks and Friends …

<b>Note:</b> I also love to leave notes all over my writing!

<b>Disclaimer:</b> This is a [test page](#) in case you have not figured out yet!

<b>Warning:</b> Das `-f` steht für "vorsichtig"!

Just to test the marigins, here is some more text!

<b>Note:</b> The top margins should also be tested against regular paragraphs.

### Quotes

Also there are some things worth quoting:

> Humans are allergic to change. They love to say, <q>We've always done it this way.</q>
> I try to fight that. That's why I have a clock on my wall that runs counter-clockwise.
> 
> <cite>[Grace Hopper](https://en.wikiquote.org/wiki/Grace_Hopper#The_Wit_and_Wisdom_of_Grace_Hopper_(1987))</cite>

However, there are also <q>Inline quotes</q> which are ssometimes useful.

### Pictures

Pictures are pretty and brains seem to like looking at them …

{{ picture(src="/assets/banner_plasma/mostly_green.webp", alt="A mostly green nebula that is used as a generic banner background.") }}

{% chat() %}
: /resources/emoji/neofox/neofox.png ::: Neofox notes:
> And click on them, to get a larger image.

> No I'm not clickable.

> There are also inline-able SVGs, [see the ATMega328P Article](/notebook/atmega328p/#building-the-circuit).
{% end %}

### Code blocks

Codeblocks are usually used for interesting code-snippets:

```sh
#!/bin/sh

echo "I'm a relatively boring shellscript!"
echo "Btw: My filename is $0"
```

They also integrate with figures:

{% figure(caption="Usually I explain what the <code>code</code> is about here", caption_after="And interesting conclusions, facts or a short explanation of the code itself go here.") %}
```lua
print("I jsut some unrelated lua example.")
-- with a comment
local with = "a variable" and 42
```
{% end %}

## Links

Links, everyone loves some links, they show the path to even more knowledge! Or sometimes are [just examples in a widget zoo](#links).

Links comes in some varieties here, not just [inline](#).

{% linkbutton(href="#") %}I'm a plain link button for important links.{% end %}

<b>Warning:</b> These linkbuttons take a lot of space and draw attenion, use sparingly.

Link buttons can be decoratated for different destinations:

{% linkbutton(href="#", icon="destination-text") %}Decorated for documentation.{% end %}

{% linkbutton(href="#", icon="destination-code") %}Decorated for `code`.{% end %}

{% linkbutton(href="#", icon="destination-listing") %}Decorated for listings.{% end %}

{% linkbutton(href="#", icon="destination-contact") %}Contact Information{% end %}

Or for different actions:

{% linkbutton(href="#", icon="action-next") %}Next{% end %}

{% linkbutton(href="#", icon="action-previous") %}Previous{% end %}

{% linkbutton(href="#", icon="action-shuffle") %}Random{% end %}

{% linkbutton(href="#", icon="action-search") %}Search{% end %}

### Link lists

When one has a lot of links it might also make sense to put them into a list:

{% linklist() %}
* [it is also possible](#)
* [to have a lot of links](#)
* [in a list, where appropriate](#)
{% end %}


!note: <b>Trivia Note:</b> This link style originated from the fact that the first prototype of the slatecave could also produce [gemtext](https://geminiprotocol.net/docs/gemtext-specification.gmi#link-lines).

## Previews

<section class="h-entry preview">
	<h3><a href="#">This is a test preview</a></h3>
	<p class="p-summary">Previews are usually more than one in their own section like it is here, this spot usually contains a paragraph of description.</p>
	<p class="metadata"><a href="#">Blog</a> 2023-11-04 (pinned)</p>
</section>

<section class="h-entry preview">
	<h3><a href="#">This is another test preview</a></h3>
	<p class="p-summary">Fusce sed tellus ac risus ornare molestie. Nunc arcu orci, blandit sit amet viverra a, congue eget purus. Proin lacinia urna eget nibh aliquet fringilla. Cras convallis odio neque.</p>
	<p class="metadata"><a href="#">Somewhere</a> 2023-11-04</p>
</section>

## Tables

While definition lists are very useful, sometimes one needs a table:

<table>
<caption>An overview of html tags.</caption>
<tr><th>Name</th><th>Syntax</th><th>Example</th></tr>
<tr>
	<td>Italics</td>
	<td><code>&lt;i&gt;text&lt;/i&gt;</code></td>
	<td><i>idiom</i></td>
</tr>
<tr>
	<td>Bold</td>
	<td><code>&lt;b&gt;text&lt;/b&gt;</code></td>
	<td><b>slightly important</b></td>
</tr>
<tr>
	<td>Quote</td>
	<td><code>&lt;q&gt;text&lt;/q&gt;</code></td>
	<td><q>Lorem Ipsum</q></td>
</tr>
<tr>
	<td>Emphasis</td>
	<td><code>&lt;em&gt;text&lt;/em&gt;</code></td>
	<td><em>watch out here</em></td>
</tr>
<tr>
	<td>Strong (Emphasis)</td>
	<td><code>&lt;strong&gt;text&lt;/strong&gt;</code></td>
	<td><strong>I really mean it!</strong></td>
</tr>
<tr>
	<td>Keypresses</td>
	<td><code>&lt;kbd&gt;text&lt;/kbd&gt;</code></td>
	<td><kbd>Super</kbd>+<kbd>Shift</kbd>+<kbd>q</kbd></td>
</tr>
</table>

<b>Note on Small Screens:</b> Tables don't work too well on small screens, depside best effords on the stylesheet side, you shouldn't let tables get too wide.

<b>Note on Screenreaders:</b> Screenreaders work great with tables as long as they aren't abused for visualizing non-table data. Consider definition lists, nested lists and sections with appropritate headlines instead.

## Conversations

Sometimes things need to be said …

{% chat() %}
: /resources/emoji/neofox/neofox_laptop.png ::: A neofox sitting in front of their laptop:
> Looks like Slatian needs some excuse to get a note in again.

: /resources/emoji/slatian_bean/slatian_bean.png ::: Slatian replies:

> Errm, no, this time I'm just testing some styling.
>
> But if you insist on a note …
>
> <b>Note:</b> Perfectly fine to put a note into a chat bubble.

> See …

: /resources/emoji/neofox/neofox_laptop_notice.png ::: Neofox warns:

> Don't overdo it with notes!
>
> And don't make too long conversations, those emoji use bandwidth, you know?
>
> <hr>
>
> > Btw. here is a quoted quote.
>
> ```lua
> -- And:
> some_quoted("code")
> ```

: /resources/emoji/slatian_bean/slatian_bean_happy_blep.png ::: Slatian bleps in response.

> …

{% end %}

### Headline for margin testing

should be okay
