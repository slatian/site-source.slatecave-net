+++
	title = "About me, Slatian"
	description = "A bit about myself and how you can contact me."
	template = "page.html"
	updated = 2024-05-05
+++

## Digital me, Slatian (sleɪtiˑan)

I'm a geek interested in building programs, websites, more or less useful systems and customising my Desktop, I like Linux and am interested in minimalist (my rough definition being: Can I understand enough of it after spending an afternoon with the manual to have an Idea of how it works and does it run on my phone?) and cleanly (usually means that a lot of parts beautifully fit together with easy to understand interfaces) built systems.

My code is (at the time of writing) usually written in Rust or Shellscript, but I also know awk, Vala, Lua, Python, JavaScript, a bit of C and x86 Assembly, some of it is also creative use of configuration mechanisms and templating.

Apparently I care enough about the web to make this website because the web needs more small personal websites!

I'm also interested in organising Information in general (And usually fail at doing exactly that …).

### Why that name?

You may also know me under my old nick Baschdel (baʃdəl) which had some issues, one of them being that it is surprisingly common (derived from Sebastian). Slatian (sleɪtiˑan/slay-tee-an) is derived from this websites name (I bought the domain about a week or two earlier) which I simply derived from one of my favourite background colours (also the Material has historically been used to record Information (extra geek factor?)).

## Contact

In general: I'm handling most of my communication that I'll reply when I have time to reply, you can message me at any day or nighttime however (just be prepared to wait a a bit longer for the answer). Also I'm OK with you trying to contact me through multiple channels if one isn't successful (I don't always look at everything).

You can talk to me in English and German.

### Fediverse

{% linklist() %}
* [@slatian@pleroma.envs.net](https://pleroma.envs.net/slatian)
* [@baschdel@fedi.absturztau.be (as backup)](https://fedi.absturztau.be/baschdel)
{% end %}

Simply mention me at any level of visibility you're comfortable with.

I used to have my own activity pub enabled server as @slatian@fedi.slatecave.net but I shut that one dowsn as I didn't use it while it was eating my servers resources.

As of 2023-11-07 I'm switching over to envs.net, Thanks to Puniko for over 3 years of service over on absturztaube!

### Matrix

You can write to [@slatian:slatecave.net](https://matrix.to/#/@slatian:slatecave.net) to reach me over Matrix.

### E-Mail

I take E-Mails over at <i>baschdel ädd disroot döt org</i>. Since my E-Mail setup is a mess I haven't setup encryption yet.

HTML is awesome, but I prefer it not being in my inbox … [please send me plaintext E-Mail.](https://useplaintext.email)


### IRC

I'm known as <i>slatian</i> on [Libera.Chat](https://libera.chat/) and [OFTC](https://oftc.net/), but I'm rarely logged in.

### Git (Merge Requests / Issues)

{% linkbutton(href="https://codeberg.org/slatian/") %}codeberg.org/slatian{% end %}

If you already have an account on Codeberg and you have an Issue or Merge Request for one of my projects hosted there you can communicate these through Gitea. Any other means of communication is fine too (including sending me a (link to a) diff over the Fediverse or Matrix!).


