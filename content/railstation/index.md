+++
	title="Links to Elsewhere - The Slatecave Railstation"
	date=2024-04-25
	[extra]
	banner="slatecave_railstation_sign"
	final_image="/resources/train_tcp_443-slatecave-net.svg"
	final_image_alt="The TCP 443 train to slatecave.net"
+++

Just like a real Railstation this page gets you to a lot places on the internet, the only differences are that you don't need a ticket and don't have to wait.

## Friendly Sites

Run by awesome people:

{% linklist() %}
* [seirdy.one by Seirdy](https://seirdy.one)
* [volpeon.ink by Volpeon](https://volpeon.ink)
* [adrian.geek.nz by Adrian Cochrane](https://adrian.geek.nz)
* [smolderg.xyz by Therra](https://smolderg.xyz)
* [steffo.dev by Steffo](https://steffo.dev)
* [finnley.dev by Finnley](https://finnley.dev)
* [envs.net, a Pubnix by creme and others](https://envs.net)
{% end %}

<small>This list is not complete for the simple reason of having to fit this list into some finite space.</small>

<b>Note of Thank you!</b> Both [seirdy.one](https://seirdy.one) and [volpeon.ink](https://volpeon.ink) inspired this website when it was created and continue to do so. The ["How does it work?" series](https://adrian.geek.nz/docs.html) is one of the main reasons for the taking software apart type posts on my blog.

Run by on-profit Organisations, providing useful services:

{% linklist() %}
* [Codeberg.org - Codeforge hosting](https://codeberg.org)
* [Disroot.org - Web services](https://disroot.org)
{% end %}


## Wall of Badges

Not a wall right now, but one has to start somewhere.

<p>
{{ webbadge(img="88x31_seirdy-one.png", href="https://seirdy.one", alt="Seirdy, simple white on black text next to a :; icon.") }}
{{ webbadge(img="volpeon-ink.svg", href="https://volpeon.ink", alt="A mysterious wyvern in a forest, the text says volpeon") }}
{{ webbadge(img="88x31_codeberg-org.png", href="https://codeberg.org", alt="Codeberg, text next to a codeberg icon.") }}
{{ webbadge(img="88x31_smolderg-xyz-by-liah.png", href="https://smolderg.xyz", alt="Smol derg, big heart.") }}
{{ webbadge(img="88x31_finnley-dev.png", href="https://finnley.dev", alt="A fox emoji, but it's a colorful arctic fox. The text says Finnley.dev") }}
{{ webbadge(img="cc-by-nc-sa.eu.svg", href="/about/#legal-foo", alt="CC BY-NC-SA") }}
</p>

## Webrings

{{ webring_widget(
	name="Envs Webring",
	headline_tag="h3",
	description="A webring for every creature on [envs.net](https://envs.net).",
	previous_uri="https://envs.net/ring/?action=prev&me=slatian",
	next_uri="https://envs.net/ring/?action=next&me=slatian",
	random_uri="https://envs.net/ring/?action=random&me=slatian",
	about_uri="https://envs.net/ring/",
	about_link_text="Envs webring homepage"
) }}

## Linking to the Slatecave

If you have a similar page and want to link back to the slatecave: Thank you!

You can use the following badges if you want to:

<p>
{{ webbadge(img="88x31_slatecave-net.png", href="https://slatecave.net", alt="slatecave.net, Behind the text there is a drawing of the head of a floofy dragon.") }}
{{ webbadge(img="88x31_slatecave-net_sleeping-slatian.png", href="https://slatecave.net", alt="A sleeping drgon drawn in warm, flat color lines on a dark background, the outline has a slight redish to orangely pastel tint") }}
</p>

<b>Note on Hotlinking:</b> If you want to hotlink (load the image from slatecave.net) feel free to do so. But please consider serving it from your own webserver.

!note: <b>Image credits:</b> Image credits for the dragon in the background of the first badge go to [Mollin](https://twitter.com/Mollin_Art) ([website](https://mollincomms.carrd.co/)), it is derived from the sketch of a dragon bean I've commissioned.

