# Slatians Hideout on the Interweb

This is a personal blog, projects and wiki site hosted at [slatecave.net](https://slatecave.net).

It is built using [the zola static site generator](https://gezola.org).

## Structure

* [content/](content) - contains most of the sites text.
	* [about/](content/about) - contains information about the site and myself, also home for the footer
	* [blog/](content/blog) - My blog
	* [creations/](content/creations) - A showcase of some things I made
	* [notebook/](content/notebook) - A public personal wiki where I note down information I find useful
* [meta/](meta) - contains text snippets used by the templating
	* [404.md](meta/404.md) - The message shown on a 404 page
	* [footer.md](meta/footer.md) - The message shown in the footer
* [static/](static) - static files, mostly assets
	* [assets](static/assets) - site theme
		* [banner_plasma](static/assets/banner_plasma/) - plasma banner theme
		* [banner_slatecave-creations](static/assets/banner_slatecave-creations/) - banners for some projects
		* [site_slatecave](static/assets/site_slatecave/) - [slatecave theme](#slatecave-theme)
	* [blog/.htaccess](static/blog/.htaccess) - old redirects, not served anymore
	* [resources/badges/cc-by-nc-sa.eu.svg](static/resources/badges/cc-by-nc-sa.eu.svg) - an optimized CC BY-NC-SA badge. currently unused.
* [templates](templates) - templates for generating the slatecave, see [templates section](#templates)
	* [shortcodes](templates/shortcodes/) - shortcodes used in the page sources
* [banners.toml](banners.toml) - contains configuration for the banner on each site.
* [build.sh](build.sh) - a script for local building, also invokes `tidy` and reports markup issues.
* [config.toml](config.toml) - [configuration file for zola](https://www.getzola.org/documentation/getting-started/configuration/)
* [report.awk](report.awk) - used in `build.sh`, summarizes the output of the `tidy` tool.
* [robots.txt](robots.txt) - used by the [robots.txt template](templates/robots.txt) to generate the robots.txt file.

## Slatecave theme

<img src="static/assets/site_slatecave/favicon.png" style="float: right" alt="The slatecave favicon">

Files:
* [icons_dark/](static/assets/site_slatecave/icons_dark) - dark mode icons
* [icons_light/](static/assets/site_slatecave/icons_light) - light mode icons
* [dragon.svg](static/assets/site_slatecave/dragon.svg) - Slate the dragon (in the uper right corner)
* [favicon.png](static/assets/site_slatecave/favicon.png) - The sites favicon
* [gallery.css](static/assets/site_slatecave/gallery.css) - Unused, probably broken
* [slatecave_v2.css](static/assets/site_slatecave/slatecave_v2.css) - Old slatecave theme (still works!)
* [slatecave_v3.css](static/assets/site_slatecave/slatecave_v3.css) - New slatecave theme

TODO: a bit more about how the theme works … in summary: semantic html with a few utility classes.

## Templates

The slatecaves templating has grown quite a bit over the last year … I'll try to keep it brief.

Zola allows templates to extend another template and overwrite predefined template blocks, because of this all html templates extend [base.html](templates/base.html) (which is quite a beast) and customize small parts of it.

Template Files:
* Special Templates:
	* [base.html](templates/base.html) - base template all page templates extend
	* [elements.html](templates/elements.html) - macros that are probably only used on slatecave.net
	* [helpers.html](templates/helpers.html) - template macro libary
	* [robots.txt](templates/robots.txt) - generates the robots.txt file
* Page Templates:
	* [404.html](templates/404.html) - template for the 404 page
	* [page.html](templates/page.html) - default page template, just extents `base.html`
	* [blog-page.html](templates/blog-page.html) - Blog pages have special titles
	* [index.html](templates/index.html) - Template of the landing page, contains some interesting feed generation
* Section Templates
	* [blog-index.html](templates/blog-index.html) - blog index, groups blog-posts by year
	* [creations-index.html)s-index.html](templates/noteboo](templates/)k-index.html) - a pretty boring section template
	* [notebook-index.html](templates/taxonomy_single.html) - currently unused template as the notebook is also a Taxonomy
* Taxonomy
	* [taxonomy_list.html](templates/taxonomy_single.html) - A generic taxonomy list that has a term overview and a section with the contained articles for each term.
	* [taxonomy_single.html](templates/taxonomy_single.html) - Not very polished listing of articles inside a term.
	* [notebook/list.html](templates/notebook/list.html) - Reders the `/notebook` page, tries to feel like a section template, but allows foregin content.

### Template configuration

`[extra]` values for sections and pages frontmatter:
* `banner` <b>string</b> Set a custom banner for the page
* `preview_image` <b>string</b> set a custom preview image for link previews using e.g. opengraph.
* `hide_description` <b>bool</b> stops the description from showing up in place of a left out first paragraph.
* `final_image` <b>string</b> Path to image file that will be put below the post
* `final_image_alt` <b>string</b> Alt-text for the `final_image`

`[extra]` values for sections frontmatter:
* `short_title` <b>string</b> with a short title that is diplayed on a buttons leading to the section
* `show_feed_link` <b>bool</b> set to true to show the feed link on the page, make sure to also set `generate_feed` in the general section. (There is also an or in case zola decides to expose the `generate_feed` option as `section.generate_feed`.)

`[extra]` values for pages:
* `pinned` <b>bool</b> if the page is pinned it will always show up on the frontpage.

`[extra]` values for creation pages frontmatter:
* `creation_status` <b>string</b> sort creation status description.
* `sourcecode_uri` <b>string</b> the uri of the repository the project lives in.
* `fork_of` <b>string</b> name of the project this one is a fork of.
* `fork_of_uri` <b>string</b> uri to the project this one is a fork of.

Note: The `date` attribute will be displayed as the project start date.

### Inline Template hints

The [base.html](templates/base.html) template does some rearranging to make the section based layout with boxes possible without nightmare markdown, around `h2` headlines. To mark sections as what they are one can place some inline hints about a section.

* `<!--H-CARD-SECTION-->` add a [`h-card` class for use with microformats](https://microformats.org/wiki/h-card)
* `<!--LANGUAGE:de-->` sets `lang="de"` on the section to mark it as being in german.

## Taxonomies

Currently the following taxonomies are configured:
* `notebook` - to place a page inside the Notebook section
	* Guides - guides and tutorials
	* Cheatsheets - Lookup tables, snippets, Cheatsheets
	* Bookmarks - A little Web directory
* `topics` - What the article is about
	* Art
	* Desktop Ricing - see also screen locking
	* Electronics and Microcontroller
	* Mobile Linux - Linux on the phone
	* Networking - general, also ssh and such
	* Shell - Everything that has to do with Cli, Tui and Shellscripting
	* Tools - Expected overlap wit Shell and Web
	* Web - Everything that has to do with web technology
* `lang` - Programming/Markup Language
	* Bash - see also Sh
	* C
	* Css
	* Html
	* JavaScript
	* Lua
	* Python
	* Rust
	* Sh - see also Bash, tag with both if relevant
	* Vala

Note: Html, Css and JavaScript only when it is directly about those things, otherwise place it in the topic Web.

## Card paragraphs

Card paragraphs are, as the name says paragraphs with additional information.

They receive the styleclass `card` along with a type like `note`, `warning` or `disclaimer`.

The base template does some prefix matching to generate note paragraphs:
* `<b>Note`
* `!note: `
* `<b>Disclaimer`
* `<b>Warning`
* `!warning: `

```markdown
<b>Note:</b> I'm a `note` type card paragraph!
```

Currently they'll get styled with a bar on the left, an accent color and the first `<b>` tag is displayed like a mini-headline.

These used to be called Extra-paragraphs and use the `extra` style class, but those get filtered out by readability.js, not great for useful or important information.

## Shortcodes

Whenever a shortcut sourrounds something the follwoing pattern is meant:

```markdown
{% shortcode() %}
*Markdown* goes [here](#)
{% end %}
```

### chat

Sourrounding helper for creating converations between emoji.

Does not accept additional parameters.

```markdown
{% chat() %}
: /resources/emoji/neofox/neofox.png ::: Neofox notes:
> This is how you create a conversation widget.
>
> You can use **markdown** inside these blocks

> This would be a second chat bubble.

: /resources/emoji/slatian_bean/slatian_bean_happy.png ::: Slatian adds:
> You can switch characters by simply starting a line with a colon (`:`)
{% end %}
```

Character definition line:
```
: <image-path> ::: <alt-text>
```

If the `<alt-text>` is missing the template will error, it should make the conversation meaningful (and fun) without the images.

### dl

Sourrounding helper for cerating definition lists, line based.

Accepts an optional parameter: `compact`, when set to `false` the compact mode enabled for when there is only a one to one mapping is always disabled.

```markdown
{% dl() %}
Defintion Title
: Definition Item
: Another Item
My _Website_
: [slatecave.net](https://slatecave.net)
{% end %}
```

### linklist

Sourrounds an ordinry markdown list, adds the `link-list` class to any `<ul>` tag inside that has no attributes.

```markdown
{% linklist() %}
* [Foo Site](https://example.org/foo)
* [Bar Article](https://example.org/bar)
* [Baz Code Repository](https://codeberg.org/slatian/site-source.slatecave-net)
{% end %}
```

### linkbutton

Creates a link button, also known as `button-paragraph` to the stylesheet, which is a paragraph with exactly one link inside.

It accepts some parameters:
* `href` mandatory, the destination url, if it starts with an `@`  [zolas `get_url` function](https://www.getzola.org/documentation/templates/overview/#get-url) will be used to generate the url.
* `txt` optional markdown text for the button, alternatively it may sourround the text it contains, if none of the two the button just displays its `href`.
* `download` se to true to make the link always download the file it points to.

```markdown

{% linkbutton(href="https://slatecave.net")%} Check out slatecave.net, it has linkbuttons! {% end %}

```

Link buttons can have icons by using the `icon` attribute.

Available icons are:
* `destination-text`
* `destination-code`
* `destination-listing`
* `destination-contact`
* `action-next`
* `action-previous`
* `action-shuffle`
* `action-search`
* `action-download`

Make sure that the linkbuttns are on their own paragraph, by leaving blank lines above and below, tidy complaining about opeing and closing p tags (as well as removing an empty p tag is a pretty good indicator that you are doing this wrong!)

### figure

Sourround a markdown block with it to make it a HTML `<figure>` element.

It accepts some parameters:
* `caption` an optional html string that will be inserted before the contant as `<figcaption>`
* `caption_after` same as `caption` but insetrd after the figure content

~~~markdown
{% figure(caption="Lua snippet that prints foo")%}
```lua
print(foo)
```
~~~

<b>Note:</b> This also inserted `<pre>` and `<code>` tags but they were removed in the favor of markdown proce4ssing and automatic syntax highlighting. Maybe it will be removed in the future to allow figures to be rendered in any markdown viewer.

### picture

Can embed colocated or abolute path images in a page and links directly to the image.

Automatically links colocated files where there is a `-light` or `-dark` before the filextension.

It accepts some parameters:
* `src` Path to image (the one that will be linked)
* `alt` Image description text

### inline_svg

Takes the path to an svg file and inlines it into the html. Make sure you replace all occourrances of the images primary forground color with `currentColor` and that the other colors play well with both, light and dark backgrounds.

It accepts some paramters:
* `path` the path passed to `load_resource()`, also accepts `./<filename>` for colocated files.
* `labels_are_incomplete_or_broken` will, when set to `true` remove all instances of `aria-label` tags from an svg. Useful when those labels don't work without seeing the image and a description is provided instead.
* `caption` will put the svg in a figure and add the given caption before.
* `caption_after` will put thesvg in a figure and add the given caption after the image.

### webbadge

Displays a badge image and can put it inside a button.

It accepts the some parameters:
* `img` image file name in [`/resources/badges/`](./resources/badges/) .
* `alt` image description for the badge.
* `href` url for linking the badge to.

Use it like this:
```html
<p>{{ webbadge(…) }}</p>
```

### webring_widget

This shortcode makes a webring widget with some fancy buttons and a frame around it.

It accepts some parameters:
* `name` mandatory, the webrings name
* `headline_tag` optional, defaults to `p`, the html tag type that should be used for the headline (i.e. `h2` or `h3`)
* `description` optional, a description of the webring displayed between name and buttons, supports markdown.
* `previous_uri` mandatory
* `next_uri` mandatory
* `random_uri` optional (if the webring supports jumping to a random site)
* `search_uri` optional (if the webring has a search engine)
* `about_uri` optional, the link to the webrings homepage
* `about_link_text` optional, custom text for the webrings homepage button
