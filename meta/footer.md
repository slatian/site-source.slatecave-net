
---

<ul class="link-list row fill">
<li><a href="/about" >About This Site</a></li>
<li><a href="/about/me" class="decoration-destination-contact">About Slatian / Contact</a></li>
<li><a href="https://codeberg.org/slatian/site-source.slatecave-net" class="decoration-destination-code">This Sites Sourcecode</a></li>
</ul>

<ul class="link-list row fill">
<li><a href="/topics">Explore by Topic</a></li>
<li><a href="/lang">Explore by Language</a></li>
<li><a href="/about/feeds" class="decoration-destination-feed">Subscribe</a></li>
</ul>

---

## Clicky Links to Elsewhere

<p><a href="https://echoip.slatecave.net">echoip.slatecave.net - IP and DNS Lookup</a></p>

<b>Search this and other personal websites:</b>
[Clew.se](https://clew.se/), [Marginalia-Search.com](https://marginalia-search.com/), [SearchMySite.net](https://searchmysite.net/), [Unobtanium.rocks](https://unobtanium.rocks/)

<b>Neofox Emoji (used on some pages):</b> [made by Volpeon, licensed under CreativeCommons CY-NC-SA 4.0](https://volpeon.ink/emojis/neofox/).

{{ linkbutton(href="/railstation", icon="action-next" txt="Even more Links: The slatecave.net Railstation") }}
