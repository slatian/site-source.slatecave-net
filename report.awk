#!/usr/bin/awk -E

BEGIN {
	searching_for_header = 1
	current_file = ""
	total_files = 0
	warnings["."]=0
	errors["."]=0
}

/===== File:/ {
	if (match($0, "===== File: (.+)",m)) {
		current_file = m[1]
		searching_for_header = 0;
		total_files++;
	}
}

/ Warning: / {
	warnings[current_file]++;
	report_content[current_file] = 1;
}

/ Error: / {
	errors[current_file]++;
	report_content[current_file] = 1;
}


/^About HTML Tidy:/ {
	searching_for_header = 1
}

(! searching_for_header) && ! /^No warnings or errors/ && ! /^Tidy found/ && ! /===== File: / {
	messages[current_file][length(messages[current_file])+1] = $0
}

END {
	print "\n===== Tidy Report Summary:\n"
	print total_files " Files processed, " length(warnings)-1 " with warnings, " length(errors)-1 " with errors."

	print  "\n=== Files with warnings or errors:\n"
	for (file in report_content) {
		print "=== File: " file
		print (warnings[file] || 0) " Warnings " (errors[file] || 0) " Errors"
		for (i = 1; i < length(messages[file]); i++) {
			print messages[file][i]
		}
	}
	
}
