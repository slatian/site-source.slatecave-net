#!/bin/sh

zola build

find public/ -name "*.html" | while read file; do
	printf "===== File: %s\n" "$file"
	tidy --indent-with-tabs yes --keep-tabs yes -wrap 0  -im "$file" 2>&1
done | grep -v 'proprietary attribute "aria-' | ./report.awk
