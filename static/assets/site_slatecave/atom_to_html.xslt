<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:atom="http://www.w3.org/2005/Atom">


<xsl:output method="html" encoding="utf-8" indent="yes" />
<xsl:template match="/">
<html lang="en">
	<xsl:variable name="title">
		<xsl:choose>
			<xsl:when test="contains(atom:feed/atom:title, ' - ')">
				<xsl:value-of select="substring-after(atom:feed/atom:title, ' - ')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="atom:feed/atom:title"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<head>
		<title><xsl:value-of select="$title"/></title>
		<meta content="width=device-width, initial-scale=1" name="viewport"/>
		<link href="/assets/site_slatecave/slatecave_v3.css" rel="stylesheet" type="text/css" title="Slatecave v3"/>
		<link href="/assets/site_slatecave/favicon.png" rel="icon" type="image/png"/>
	</head>
	<body>
		<header>
			<nav>
				<a class="sitename" href="/">slatecave.net</a>
				<ul class="link-list row">
					<li>
						<a class="decoration-action-previous">
							<xsl:attribute name="href">
								<xsl:value-of select="atom:feed/atom:link[@rel='alternate' and @type='text/html']/@href"/>
							</xsl:attribute>
							Back to <xsl:value-of select="$title"/>
						</a>
					</li>
					<li>
						<a href="/about/feeds" class="decoration-destination-feed">All Feeds</a>
					</li>
				</ul>
			</nav>
		</header>
		<article>
		<h1><xsl:value-of select="$title"/> / Feed Preview</h1>
		<p>You can subscribe to this feed by pasting the <a rel="self"><xsl:attribute name="href"><xsl:value-of select="atom:feed/atom:link[@rel='self']/@href"/></xsl:attribute>URL of this page</a> into your Feedreader.</p>
			<section>
				<h2>Feed Entries</h2>
		<xsl:for-each select="atom:feed/atom:entry">
			<section class="preview">
				<h3><a>
					<xsl:attribute name="href">
						<xsl:value-of select="atom:link[@rel='alternate' and @type='text/html']/@href"/>
					</xsl:attribute>
					<xsl:value-of select="atom:title"/>
				</a></h3>
				<p class="p-summary"><xsl:value-of select="atom:summary"/></p>
				<p class="metadata">
					<xsl:variable name="entry_published" select="substring-before(atom:published,'T')"/>
					<xsl:variable name="entry_updated" select="substring-before(atom:updated,'T')"/>
					Published: <time><xsl:value-of select="$entry_published"/></time>
					<xsl:if test="not($entry_published = $entry_updated)">,
						Updated: <time><xsl:value-of select="$entry_updated"/></time>
					</xsl:if>
				</p>
			</section>
		</xsl:for-each>
			</section>
			<section>
				<h2>If this is a feed, why does it look like a Webpage?</h2>
				<p>The short Answer: <a href="https://www.w3schools.com/xml/xml_xslt.asp">XSLT</a></p>
				<p>You might be familiar with your Browser telling you that there is no stylesheet for the displayed XML. It doesn't mean CSS, it means XSLT.</p>
				<p>This allows taking the feeds XML apart and applying some XML based templating and put it back together as HTML. All of this happens in the browser!</p>
				<p>I've written a <a href="/blog/atom-xslt/">Blogpost on how this preview works</a> in case you are interested.</p>
			</section>
		</article>
	</body>
</html>
</xsl:template>

</xsl:stylesheet>
